/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include <iostream>
#include <string>
#include "TranscodingChannel.h"
using namespace std;



/*-----------------------------------------------------------------------------
Function name:
Author/s:
Current Version:

Version history: version history of function.

Description:	Description of the function

Parameters:
Inputs:

Outputs:

Returns: If function is not void, describe the return.

Comments: Additional comments regarding function. Possible optimizations, errors, things to investigate, etc...
//TODO: Encoding and decoding steps should be invoked only after specific status is reached. User story: In
decoding file was not open correctly and decoding steps were done as everything is OK.

-----------------------------------------------------------------------------*/


int main(int argc, char* argv[])
{
	if (argc > 1)
	{
		cout << "Running the example application for Bolt65 that uses libraries." << endl;

		TranscodingChannel ch1;
		ch1.StartTranscoding(argv, argc);
	}
	else
	{
		cout << "Error in passing of arguments." << endl;
	}

	return 0;

}