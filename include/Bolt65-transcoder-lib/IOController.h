/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once

#include "ioBlock.h"
#include "EncodingContext.h"
#include "Config.h"
#include "Frame.h"
#include "EntropyBuffer.h"
#include "MemoryManager.h"

namespace transcoder
{
	class IOController
	{
	public:
		string path;

		IOController();
		IOController(Config *config);
		~IOController();

		void InitIO(Config * config, EncodingContext * enCtx, EncodingContext *decCtx);
		int writeBufferToFile(EntropyBuffer *buffer);
		void closeFiles();

	private:

		void determineInputFileType(string inputFileName);

		FILE *inputFilePointer;
		ofstream outputFile;

		ioBlock ioKernel;
		int frameNumber;

		int inputBufSize;
		int outputBufSize;

	};
}