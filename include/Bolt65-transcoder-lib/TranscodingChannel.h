/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "Config.h"
#include "EncodingContext.h"
#include "../../include/Bolt65-decoder-lib/EntropyController.h"
#include "../../include/Bolt65-encoder-lib/EntropyController.h"
#include "ConfigurationManager.h"
#include "Enums.h"
#include "DPB.h"
#include "DPBManager.h"
#include "Scanner.h"
#include "Statistics.h"
#include "PartitionProcessor_D.h"
#include "PartitionProcessor_E.h"
#include "IOController.h"
#include "ResolutionConverter.h"
#include "LoadBalancing.h"

class TranscodingChannel
{
public:
	TranscodingChannel();
	void StartTranscoding(char* argv[], int argc);
	void Configure(char* argv[], int argc);
	void OpenIOStreams();
	void Initialize();
	void Run();
	~TranscodingChannel();

private:
	TRANSCODER_STATUS STATUS;
	Config configuration;
	EncodingContext enCtx;
	EncodingContext decCtx;

	Streamer *streamObject;

	transcoder::PartitionProcessor_E **encPartitionProcessors;
	bool encProcessesInitialized = false;

	transcoder::PartitionProcessor_D **decPartitionProcessors;
	bool decProcessesInitialized = false;

	int decNumOfPartitions;
	int **decTilesPerThread;

	int encNumOfPartitions;
	int **encTilesPerThread;
	

	decoder::EntropyController decoderEntropyC;
	encoder::EntropyController encoderEntropyC;

	ConfigurationManager cc;
	transcoder::IOController transcoderIO;

	DPBManager dpbmDecoder;
	DPBManager dpbmEncoder;

	Statistics stats;

	bool addNewVPS = true;
	bool addNewSPS = true;
	bool addNewPPS = true;

	void ThreadDecProcessor(int threadId, Frame *frame);
	void ThreadEncProcessor(int threadId, Frame * frame, Frame *decFrame);
	void InitializeEncProcesses();
	void InitializeDecProcesses();
	void clearMemory();
	void printTranscodingInfo();

	void checkExternalConfig(Config &config, char* argv[], int argc);
	void parseConsoleParameters(Config &config, char* argv[], int argc);

	void decodeFrame(Frame *decFrame, int decFrameNum);
	void encodeFrame(Frame *encFrame, Frame *decFrame, int decFrameNum);

};

