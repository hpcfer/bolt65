/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "../../include/Bolt65-encoder-lib/CTBController.h"
#include "../../include/Bolt65-encoder-lib/PredictionController.h"
#include "../../include/Bolt65-encoder-lib/EntropyController.h"
#include "TQController.h"
#include "Scanner.h"
#include "InLoopFilterController.h"

using namespace encoder;

namespace transcoder
{
	class PartitionProcessor_E
	{
	public:
		Partition *partition;
		Partition *decodedFrame;
		
		encoder::EntropyController ec;

		PartitionProcessor_E();
		~PartitionProcessor_E();

		void processPartition();
		vector <unsigned char> *getBuffer();
		void initPartition(Partition *_partition, Partition * _decodedFrame, EncodingContext *enCtx, EncodingContext *decCtx, DPBManager *dpbm);

	private:
		encoder::CTBController ctb;
		encoder::PredictionController pc;
		TQController tq;
		InLoopFilterController loopc;
		bool reuseDecodedData = false;
	};
}