/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __AVX2__
#pragma once
#include "ITransformAndQuantization.h"
#include <immintrin.h>

using namespace std;

class TransformAndQuantizationAVX : public ITransformAndQuantization
{
public:
	TransformAndQuantizationAVX();
	virtual void quantization(int16_t * A, int QP, int NB, int N, bool * is_zero_matrix, int16_t * C);
	virtual void dequantization(int16_t * A, int QP, int NB, int N, int16_t * C);
	virtual void multiply(int16_t * A, int16_t * B, int N, int16_t * C, int bdShift);
private:
	void transpose4x4(int * mat, int * matT);
	void transpose8x8(int * mat, int * matT);
	void transpose16x16(int * mat, int * matT);
	void transpose32x32(int * mat, int * matT);
};

#endif 
