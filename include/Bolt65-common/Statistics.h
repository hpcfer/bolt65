/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once 
#include <iostream>
#include <iomanip>
#include <chrono>
#include <fstream>
#include "Policy.h"
#include "EncodingContext.h"
#include "Config.h"
#include "Frame.h"
#include "ComUtil.h"


using namespace std;
class Statistics
{
public:
	clock_t begin_frame = NULL;
	clock_t begin_video = NULL;
	ofstream outputStatsTiles;
	bool outputStatsTilesExist = false;

	clock_t begin_enc = NULL; //Calculates encoder part of the transcoder
	clock_t begin_dec = NULL; //Calculates decoder part of the transcoder
	float encSum = 0; //calculates just processing part, without memory deallocation on the encoder part
	float decSum = 0; //calculates just processing part, without memory deallocation on the decoder part

	clock_t begin_test = NULL;
	float testSum = 0;

	double sumPSNRY = 0.0;
	double sumPSNRU = 0.0;
	double sumPSNRV = 0.0;
	long long sumBits = 0;
	long long frameBits = 0;

	bool showTestTileStats = false;
	float overhead = 0.0;
	float efficiency = 0.0;
	float efficiencyIdeal = 0.0;
	int efficiencyCounter = 0;

	void calculateTileLBEfficiency(float* threadData, int numOfThreads);
	void showVideoStats(EncodingContext *enCtx, Config *configuration);
	void showVideoStatsTranscoding(EncodingContext *enCtx, EncodingContext *decCtx, Config *configuration);
	void showVideoTime(Config *configuration);
	void calculateFrameStats(Frame *frame, EncodingContext *enCtx, Config *configuration);
	void showFrameTime(Config *configuration);
	void outputTileStatsToFile(bool performOutput, string outputFileName);
	void startVideoClock(bool calculate);
	void startFrameClock(bool calculate);
	void startEncodingClock(bool calculate);
	void startDecodingClock(bool calculate);
	void addToEncodingClock();
	void addToDecodingClock();
	void startTestClock();
	void addToTestClock();
	void clear(bool outputStatsTileToFile);

	Statistics();
	~Statistics();

};