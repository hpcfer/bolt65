/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
enum syntaxElemEnum {
	// General NAL unit syntax
	emulation_prevention_three_byte,

	// NAL unit header syntax
	forbidden_zero_bit,
	nal_unit_type,
	nuh_layer_id,
	nuh_temporal_id_plus1,

	// Video parameter set RBSP syntax
	vps_video_parameter_set_id,
	vps_base_layer_internal_flag,
	vps_base_layer_available_flag,
	vps_max_layers_minus1,
	vps_max_sub_layers_minus1,
	vps_temporal_id_nesting_flag,
	vps_reserved_0xffff_16bits,
	vps_sub_layer_ordering_info_present_flag,
	vps_max_dec_pic_buffering_minus1,
	vps_max_num_reorder_pics,
	vps_max_latency_increase_plus1,
	vps_max_layer_id,
	vps_num_layer_sets_minus1,
	layer_id_included_flag,
	vps_timing_info_present_flag,
	vps_num_units_in_tick,
	vps_time_scale,
	vps_poc_proportional_to_timing_flag,
	vps_num_ticks_poc_diff_one_minus1,
	vps_num_hrd_parameters,
	hrd_layer_set_idx,
	cprms_present_flag,
	vps_extension_flag,
	vps_extension_data_flag,

	//Sequence parameter set RBSP syntax
	//General sequence parameter set RBSP syntax
	sps_video_parameter_set_id,
	sps_max_sub_layers_minus1,
	sps_temporal_id_nesting_flag,
	sps_seq_parameter_set_id,
	chroma_format_idc,
	separate_colour_plane_flag,
	pic_width_in_luma_samples,
	pic_height_in_luma_samples,
	conformance_window_flag,
	conf_win_left_offset,
	conf_win_right_offset,
	conf_win_top_offset,
	conf_win_bottom_offset,
	bit_depth_luma_minus8,
	bit_depth_chroma_minus8,
	log2_max_pic_order_cnt_lsb_minus4,
	sps_sub_layer_ordering_info_present_flag,
	sps_max_dec_pic_buffering_minus1,
	sps_max_num_reorder_pics,
	sps_max_latency_increase_plus1,
	log2_min_luma_coding_block_size_minus3,
	log2_diff_max_min_luma_coding_block_size,
	log2_min_luma_transform_block_size_minus2,
	log2_diff_max_min_luma_transform_block_size,
	max_transform_hierarchy_depth_inter,
	max_transform_hierarchy_depth_intra,
	scaling_list_enabled_flag,
	sps_scaling_list_data_present_flag,
	amp_enabled_flag,
	sample_adaptive_offset_enabled_flag,
	pcm_enabled_flag,
	pcm_sample_bit_depth_luma_minus1,
	pcm_sample_bit_depth_chroma_minus1,
	log2_min_pcm_luma_coding_block_size_minus3,
	log2_diff_max_min_pcm_luma_coding_block_size,
	pcm_loop_filter_disabled_flag,
	num_short_term_ref_pic_sets,
	long_term_ref_pics_present_flag,
	num_long_term_ref_pics_sps,
	lt_ref_pic_poc_lsb_sps,
	used_by_curr_pic_lt_sps_flag,
	sps_temporal_mvp_enabled_flag,
	strong_intra_smoothing_enabled_flag,
	vui_parameters_present_flag,
	sps_extension_present_flag,
	sps_range_extension_flag,
	sps_multilayer_extension_flag,
	sps_3d_extension_flag,
	sps_extension_5bits,
	sps_extension_data_flag,

	// Sequence parameter set range extension syntax
	transform_skip_rotation_enabled_flag,
	transform_skip_context_enabled_flag,
	implicit_rdpcm_enabled_flag,
	explicit_rdpcm_enabled_flag,
	extended_precision_processing_flag,
	intra_smoothing_disabled_flag,
	high_precision_offsets_enabled_flag,
	persistent_rice_adaptation_enabled_flag,
	cabac_bypass_alignment_enabled_flag,

	// Picture parameter set RBSP syntax
	// General picture parameter set RBSP syntax
	pps_pic_parameter_set_id,
	pps_seq_parameter_set_id,
	dependent_slice_segments_enabled_flag,
	output_flag_present_flag,
	num_extra_slice_header_bits,
	sign_data_hiding_enabled_flag,
	cabac_init_present_flag,
	num_ref_idx_l0_default_active_minus1,
	num_ref_idx_l1_default_active_minus1,
	init_qp_minus26,
	constrained_intra_pred_flag,
	transform_skip_enabled_flag,
	cu_qp_delta_enabled_flag,
	diff_cu_qp_delta_depth,
	pps_cb_qp_offset,
	pps_cr_qp_offset,
	pps_slice_chroma_qp_offsets_present_flag,
	weighted_pred_flag,
	weighted_bipred_flag,
	transquant_bypass_enabled_flag,
	tiles_enabled_flag,
	entropy_coding_sync_enabled_flag,
	num_tile_columns_minus1,
	num_tile_rows_minus1,
	uniform_spacing_flag,
	column_width_minus1,
	row_height_minus1,
	loop_filter_across_tiles_enabled_flag,
	pps_loop_filter_across_slices_enabled_flag,
	deblocking_filter_control_present_flag,
	deblocking_filter_override_enabled_flag,
	pps_deblocking_filter_disabled_flag,
	pps_beta_offset_div2,
	pps_tc_offset_div2,
	pps_scaling_list_data_present_flag,
	lists_modification_present_flag,
	log2_parallel_merge_level_minus2,
	slice_segment_header_extension_present_flag,
	pps_extension_present_flag,
	pps_range_extension_flag,
	pps_multilayer_extension_flag,
	pps_3d_extension_flag,
	pps_extension_5bits,
	pps_extension_data_flag,

	// Picture parameter set range extension syntax
	log2_max_transform_skip_block_size_minus2,
	cross_component_prediction_enabled_flag,
	chroma_qp_offset_list_enabled_flag,
	diff_cu_chroma_qp_offset_depth,
	chroma_qp_offset_list_len_minus1,
	cb_qp_offset_list,
	cr_qp_offset_list,
	log2_sao_offset_scale_luma,
	log2_sao_offset_scale_chroma,

	// Access unit delimiter RBSP syntax
	pic_type,

	// Filler data RBSP syntax
	ff_byte,

	// RBSP slice segment trailing bits syntax
	cabac_zero_word,

	// RBSP trailing bits syntax
	rbsp_stop_one_bit,
	rbsp_alignment_zero_bit,

	//	Byte alignment syntax
	alignment_bit_equal_to_one,
	alignment_bit_equal_to_zero,

	// Profile, tier and level syntax
	general_profile_space,
	general_tier_flag,
	general_profile_idc,
	general_profile_compatibility_flag,
	general_progressive_source_flag,
	general_interlaced_source_flag,
	general_non_packed_constraint_flag,
	general_frame_only_constraint_flag,
	general_max_12bit_constraint_flag,
	general_max_10bit_constraint_flag,
	general_max_8bit_constraint_flag,
	general_max_422chroma_constraint_flag,
	general_max_420chroma_constraint_flag,
	general_max_monochrome_constraint_flag,
	general_intra_constraint_flag,
	general_one_picture_only_constraint_flag,
	general_lower_bit_rate_constraint_flag,
	general_reserved_zero_34bits,
	general_reserved_zero_43bits,
	general_inbld_flag,
	general_reserved_zero_bit,
	general_level_idc,
	sub_layer_profile_present_flag,
	sub_layer_level_present_flag,
	reserved_zero_2bits,
	sub_layer_profile_space,
	sub_layer_tier_flag,
	sub_layer_profile_idc,
	sub_layer_profile_compatibility_flag,
	sub_layer_progressive_source_flag,
	sub_layer_interlaced_source_flag,
	sub_layer_non_packed_constraint_flag,
	sub_layer_frame_only_constraint_flag,
	sub_layer_max_12bit_constraint_flag,
	sub_layer_max_10bit_constraint_flag,
	sub_layer_max_8bit_constraint_flag,
	sub_layer_max_422chroma_constraint_flag,
	sub_layer_max_420chroma_constraint_flag,
	sub_layer_max_monochrome_constraint_flag,
	sub_layer_intra_constraint_flag,
	sub_layer_one_picture_only_constraint_flag,
	sub_layer_lower_bit_rate_constraint_flag,
	sub_layer_reserved_zero_34bits,
	sub_layer_reserved_zero_43bits,
	sub_layer_inbld_flag,
	sub_layer_reserved_zero_bit,
	sub_layer_level_idc,

	// Scaling list data syntax
	scaling_list_pred_mode_flag,
	scaling_list_pred_matrix_id_delta,
	scaling_list_dc_coef_minus8,
	scaling_list_delta_coef,

	// Supplemental enhancement information message syntax
	last_payload_type_byte,
	last_payload_size_byte,

	// Slice segment header syntax
	// General slice segment header syntax
	first_slice_segment_in_pic_flag,
	no_output_of_prior_pics_flag,
	slice_pic_parameter_set_id,
	dependent_slice_segment_flag,
	slice_segment_address,
	slice_reserved_flag,
	slice_type,
	pic_output_flag,
	colour_plane_id,
	slice_pic_order_cnt_lsb,
	short_term_ref_pic_set_sps_flag,
	short_term_ref_pic_set_idx,
	num_long_term_sps,
	num_long_term_pics,
	lt_idx_sps,
	poc_lsb_lt,
	used_by_curr_pic_lt_flag,
	delta_poc_msb_present_flag,
	delta_poc_msb_cycle_lt,
	slice_temporal_mvp_enabled_flag,
	slice_sao_luma_flag,
	slice_sao_chroma_flag,
	num_ref_idx_active_override_flag,
	num_ref_idx_l0_active_minus1,
	num_ref_idx_l1_active_minus1,
	mvd_l1_zero_flag,
	cabac_init_flag,
	collocated_from_l0_flag,
	collocated_ref_idx,
	five_minus_max_num_merge_cand,
	slice_qp_delta,
	slice_cb_qp_offset,
	slice_cr_qp_offset,
	cu_chroma_qp_offset_enabled_flag,
	deblocking_filter_override_flag,
	slice_deblocking_filter_disabled_flag,
	slice_beta_offset_div2,
	slice_tc_offset_div2,
	slice_loop_filter_across_slices_enabled_flag,
	num_entry_point_offsets,
	offset_len_minus1,
	entry_point_offset_minus1,
	slice_segment_header_extension_length,
	slice_segment_header_extension_data_byte,

	// Reference picture list modification syntax
	ref_pic_list_modification_flag_l0,
	list_entry_l0,
	ref_pic_list_modification_flag_l1,
	list_entry_l1,

	// Weighted prediction parameters syntax
	luma_log2_weight_denom,
	delta_chroma_log2_weight_denom,
	luma_weight_l0_flag,
	chroma_weight_l0_flag,
	delta_luma_weight_l0,
	luma_offset_l0,
	delta_chroma_weight_l0,
	delta_chroma_offset_l0,
	luma_weight_l1_flag,
	chroma_weight_l1_flag,
	delta_luma_weight_l1,
	luma_offset_l1,
	delta_chroma_weight_l1,
	delta_chroma_offset_l1,

	// Short-term reference picture set syntax
	inter_ref_pic_set_prediction_flag,
	delta_idx_minus1,
	delta_rps_sign,
	abs_delta_rps_minus1,
	used_by_curr_pic_flag,
	use_delta_flag,
	num_negative_pics,
	num_positive_pics,
	delta_poc_s0_minus1,
	used_by_curr_pic_s0_flag,
	delta_poc_s1_minus1,
	used_by_curr_pic_s1_flag,

	// Slice segment data syntax
	// General slice segment data syntax
	end_of_slice_segment_flag,
	end_of_subset_one_bit,

	// Sample adaptive offset syntax
	sao_merge_left_flag,
	sao_merge_up_flag,
	sao_type_idx_luma,
	sao_type_idx_chroma,
	sao_offset_abs,
	sao_offset_sign,
	sao_band_position,
	sao_eo_class_luma,
	sao_eo_class_chroma,

	// Coding quadtree syntax
	split_cu_flag,

	// Coding unit syntax
	cu_transquant_bypass_flag,
	cu_skip_flag,
	pred_mode_flag,
	part_mode,
	pcm_flag,
	pcm_alignment_zero_bit,
	prev_intra_luma_pred_flag,
	mpm_idx,
	rem_intra_luma_pred_mode,
	intra_chroma_pred_mode,
	rqt_root_cbf,

	// Prediction unit syntax
	merge_idx,
	merge_flag,
	inter_pred_idc,
	ref_idx_l0,
	mvp_l0_flag,
	ref_idx_l1,
	mvp_l1_flag,

	// PCM sample syntax
	pcm_sample_luma,
	pcm_sample_chroma,

	// Transform tree syntax
	split_transform_flag,
	cbf_cb,
	cbf_cr,
	cbf_luma,
	abs_mvd_greater0_flag,
	abs_mvd_greater1_flag,
	abs_mvd_minus2,
	mvd_sign_flag,

	// Transform unit syntax
	cu_qp_delta_abs,
	cu_qp_delta_sign_flag,
	cu_chroma_qp_offset_flag,
	cu_chroma_qp_offset_idx,
	transform_skip_flag,
	explicit_rdpcm_flag,
	explicit_rdpcm_dir_flag,
	last_sig_coeff_x_prefix,
	last_sig_coeff_y_prefix,
	last_sig_coeff_x_suffix,
	last_sig_coeff_y_suffix,
	coded_sub_block_flag,
	sig_coeff_flag,
	coeff_abs_level_greater1_flag,
	coeff_abs_level_greater2_flag,
	coeff_sign_flag,
	coeff_abs_level_remaining,

	// Cross-component prediction syntax
	log2_res_scale_abs_plus1,
	res_scale_sign_flag,

	//VUI parameters
	aspect_ratio_present_flag,
	aspect_ratio_idc,
	sar_width,
	sar_height,
	overscan_info_present_flag,
	video_signal_type_present_flag,
	chroma_loc_info_present_flag,
	neutral_chroma_indication_flag,
	field_seq_flag,
	frame_field_info_present_flag,
	default_display_window_flag,
	vui_timing_info_present_flag,
	vui_num_units_in_tick,
	vui_time_scale,
	vui_poc_proportional_to_timing_flag,
	vui_num_ticks_poc_diff_one_minus1,
	vui_hrd_parameters_present_flag,
	bitstream_restriction_flag
};

enum arithCoderMode
{
	nd = 96,			// ctxInc value not defined yet, depends on side information  
	bypass = 97,
	toTerminate = 98,	// terminate is reserved word and can't be used
	na = 99				// not applicable, ctxInx at designated bin position doesn't exist since position is out of bin string length
};

enum NALType : unsigned char {
	/*VCL NAL Unit types - uses 6 bits*/

	/*Trailing pictures (N is non-reference, R is reference) */
	TRAIL_N = 0x00,
	TRAIL_R = 0x01,
	TSA_N = 0x02,
	TSA_R = 0x03,
	STSA_N = 0x04,
	STSA_R = 0x05,

	/*Leading pictures*/
	RADL_N = 0x06,
	RADL_R = 0x07,
	RASL_N = 0x08,
	RASL_R = 0x09,

	/*IRAP pictures*/
	BLA_W__LP = 0x10, //May have leading pictures
	BLA_W_RADL = 0x11, //May have RADL leading
	BLA_N_LP = 0x12, //Without leading pictures
	IDR_W_RADL = 0x13, //May have leading pictures
	IDR_N_LP = 0x14, //Without leading pictures
	CRA = 0x15, //May have leading pictures

	RSV_IRAP_VCL22 = 0x16,
	RSV_IRAP_VCL23 = 0x17,

	/*non-VCL NAL Unit types*/
	VPS_NUT = 0x20, //Video parameter set
	SPS_NUT = 0x21, //Sequence parameter set
	PPS_NUT = 0x22, //Picture parameter set
	AUD_NUT = 0x23, //Access unit delimiter
	EOS_NUT = 0x24, //End of sequence
	EOB_NUT = 0x25, //end of bitstream
	FD_NUT = 0x26, //Filler data
	PREFIX_SEI_NUT = 0x27, //Supplemental enhacement information
	SUFIX_SEI_NUT = 0x28

};

enum SyntaxUnitType {
	RBSPTrailingBits,
	RBSPSliceSegmentTrailingBits,
	ProfileTierLevel,
	ScalingListData,
	STRefPicSet,
	VideoParameterSetRBSP,
	SequenceParameterSetRBSP,
	PictureParameterSetRBSP,
	AcessUnitDelimiterRBSP,
	EndOfSeqRBSP,
	EndOfBitstreamRBSP,
	FillerDataRBSP,
	ByteAligment,
	SliceSegmentHeader,
	SliceSegmentData,
	SliceSegmentLayerRBSP,
	RefPicListsModification,
	PredWeightTable,
	CodingTreeUnit,
	SampleAdaptiveOffset,
	CodingQuadtree,
	CodingUnit,
	PredictionUnit,
	TransformTree,
	TransformUnit,
	PCMSample,
	MVDCoding,
	ResidualCoding,
	CrossCompPred,
	NALUnitHeader,
	NALUnit,
	ByteStreamNalUnit

};

enum PredMode
{
	MODE_INTER,
	MODE_INTRA,
	UNKNOWN
};

enum PartitionModeIntra
{
	INTRA_PART_2Nx2N,
	INTRA_PART_NxN, 
	PART_INTRA_UNDEFINED
	
};

enum PartitionModeInter
{
	PART_2Nx2N,
	PART_2NxN,
	PART_Nx2N,
	PART_NxN,
	PART_2NxnU,
	PART_2NxnD,
	PART_nLx2N,
	PART_nRx2N, 
	PART_INTER_UNDEFINED
};

enum ENCODER_STATUS
{
	NOT_STARTED,
	IO_STREAMS_OPENED,
	INITIALIZED,
	IN_PROGRESS,
	LAST_FRAME_ENCODED,
	TERMINATED
};

enum DECODER_STATUS
{
	DEC_NOT_STARTED,
	DEC_IO_STREAMS_OPENED,
	DEC_INITIALIZED,
	DEC_IN_PROGRESS,
	DEC_LAST_FRAME_DECODED,
	DEC_TERMINATED
};

enum TRANSCODER_STATUS
{
	TRANSCODER_NOT_STARTED,
	TRANSCODER_IO_STREAMS_OPENED,
	TRANSCODER_INITIALIZED,
	TRANSCODER_IN_PROGRESS,
	TRANSCODER_LAST_FRAME_DECODED,
	TRANSCODER_TERMINATED
};

enum ScanType
{
	DiagonalScan = 0,
	HorizontalScan = 1,
	VerticalScan = 2
};

enum InputFormatType
{
	YUV,
	Y4M
};

enum SliceType
{
	B = 0,
	P = 1,
	I = 2
};

enum InterpredictionMode
{
	AMVP,
	Merge,
	Skip,
	Unknown
};

enum DeblockinFilterType
{
	VERTICAL_DEB,
	HORIZONTAL_DEB
};

enum ColorIdx
{
	Luma = 0,
	ChromaCb = 1,
	ChromaCr = 2
};

enum ApplicationType
{
	Encoder = 0,
	Decoder = 1,
	Transcoder = 2,
	Test = 3
};

enum Neighbour
{
	Left = 0,
	Above = 1
};

enum SearchAlgorithm
{
	FullIntSearch = 0,
	FullFractionalSearch = 1,
	ZeroVector = 2,
	TSS = 3,
	TakeLast = 4, 
	TRANSWeightedMV = 5
};

enum BinarizationType
{
	TruncatedUnary = 0,
	TruncatedRice = 1,
	ExpGolomb = 2,
	FixedLength = 3,
	Special = 4,
	NoBinarization
};

enum BinarizationAdditionalOperation
{
	None = 0,
	PrecalculateValue = 1,
	PrecalculateParameter = 2,
	Write34Bits = 3,
	write43Bits = 4,
	RefreshContext = 5
};

enum InterpolationAlgorithm
{
	HEVCStandard = 0,
	HEVCSF5 = 1,
	AVCStandard = 2,
	Bilinear = 3,
	FourTap = 4
};

enum BlockMatchingCriteria
{
	SAD = 0,
	SATD = 1,
	MSE = 2
};

enum PartitionType
{
	FRAME = 0,
	TILE = 1
};

enum TileLoadBalancing
{
	Simple = 0,
	MaxAndMin = 1,
	SortSnake = 2,
	SortSnakeBits = 3,
	ShiftBordersBits = 4,
	ShiftBordersTimes = 5,
	ShiftBordersBitsSeachPoints = 6,
	ShiftBordersBitsSeachPoints2 = 7,
	ArticleAlgorithm = 8,
	TranscodingLB = 9
};

struct VarianceMean
{
	float variance;
	float mean;
};

struct RatioCU
{
	double RatioInCU;
	double PercentageOfWhole;
};

struct ThresholdMV
{
	bool isSimilar;
	double avgDiff;
};
	