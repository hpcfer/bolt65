/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "Frame.h"
#include "Config.h"
#include "Statistics.h"
#include <thread>
#include <algorithm>
#include <vector>
#include <iostream>



class LoadBalancing
{
public:
	static void performTileLoadBalancing(Frame * frame, int **tilesPerThread, int numOfPartitions, Frame *referenceFrame, EncodingContext *enCtx, Config *config, Statistics *stats, bool &addNewPPS);
	static void simplestTilePerThreadLB(Frame * frame, int **tilesPerThread, int numOfThreads, int numOfPartitions, Frame *referenceFrame, Statistics *stats);
	static void takeMaxAndMin(Frame * frame, int **tilesPerThread, int numOfThreads, int numOfPartitions, Frame *referenceFrame, Statistics *stats);
	static void sortSnake(Frame * frame, int **tilesPerThread, int numOfThreads, int numOfPartitions, Frame *referenceFrame, Statistics *stats);
	static void sortSnakeBits(Frame * frame, int **tilesPerThread, int numOfThreads, int numOfPartitions, Frame *referenceFrame, Statistics *stats);
	static void shiftBorderBits(Frame * frame, int **tilesPerThread, int numOfPartitions, Frame *referenceFrame, EncodingContext *enCtx, Config *config, bool &addNewPPS, Statistics *stats);
	static void shiftBorderTimes(Frame * frame, int **tilesPerThread, int numOfPartitions, Frame *referenceFrame, EncodingContext *enCtx, Config *config, bool &addNewPPS, Statistics *stats);
	static void shiftBorderBitsSearchPoints(Frame * frame, int **tilesPerThread, int numOfPartitions, Frame *referenceFrame, EncodingContext *enCtx, Config *config, bool &addNewPPS, Statistics *stats);
	static void shiftBorderBitsSearchPoints2(Frame * frame, int **tilesPerThread, int numOfPartitions, Frame *referenceFrame, EncodingContext *enCtx, Config *config, bool &addNewPPS, Statistics *stats);
	static void articleAlgorithm(Frame * frame, int **tilesPerThread, int numOfPartitions, Frame *referenceFrame, EncodingContext *enCtx, Config *config, bool &addNewPPS, Statistics *stats);
	static void transcodingBalance(Frame * decFrame, int **tilesPerThread, int decNumOfPartitions, Frame *referenceFrame, EncodingContext *enCtx, Config *config, bool &addNewPPS, Statistics *stats);
	static void reinitializeTilesPerThread(int **tilesPerThread, int numOfThreads, int numOfPartitions);
	LoadBalancing();
	~LoadBalancing();
};