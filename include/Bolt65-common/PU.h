/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "PB.h"
#include "Enums.h"
#include <vector>

class PU
{
public:

	PB pbY;
	PB pbU;
	PB pbV;
	int index;
	std::vector<std::pair<RatioCU, PU*>> transMappedPUs;
	int majorPUIndex;

	InterpredictionMode predictionMode = InterpredictionMode::Unknown;
	
	/*Motion vector format:
	bits - xxxxxyy

	last two bit yy define fractional motion vector which needs interpolation
	first bits xxxx define integer part of motion vector 

	So if motion vector is 3 it will be
	1100

	If motion vector is 3,5 or 3,23
	1110, 1101 respectively

	For chroma it will be 
	xxxxyyy (since it can be on eight part of pixel)

	*/

	//Motion vector 1 
	int MVx1;
	int MVy1;
	int MVt1;

	//Motion vector 2 in case of bi-prediction
	int MVx2;
	int MVy2;
	int MVt2;
	
	//Motion difference 1 
	int MDabsx1;
	int MDabsy1;
	int MDsignx1;
	int MDsigny1;

	//Motion difference 2 in case of bi-prediction
	int MDabsx2;
	int MDabsy2;
	int MDsignx2;
	int MDsigny2;

	int candidateIndex;
	bool isProcessed = false;
	

	PU(PB pbY, PB pbU, PB pbV);
	PU();
	~PU();
	void sort();
};

