/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include <math.h> 
#include <stdlib.h>
#include "Enums.h"
#include "TU.h"
#include "EncodingContext.h"
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>

#ifdef __AVX2__
#include "immintrin.h"
#endif

#ifdef __ARM_NEON
#include <arm_neon.h>
#endif

#ifdef __ARM_FEATURE_SVE
#include <arm_sve.h>
#endif

class ComUtil
{
public:
	~ComUtil();

	static int SAD(unsigned char * block_1, unsigned char * block_2, int puWidth, int puHeight);
	static int MSE(unsigned char* block_1, unsigned char* block_2, int puWidth, int puHeight);
	static int SATD(unsigned char * block_1, unsigned char * block_2, int blockWidth, int blockHeight);

#ifdef __AVX2__
	static int SADAVX(unsigned char * original, unsigned char * predicted, int puWidth, int puHeight);
#endif
#ifdef __ARM_NEON
	static int SADNEON(unsigned char * original, unsigned char * predicted, int puWidth, int puHeight);
#endif
#ifdef __ARM_FEATURE_SVE
	static int SADSVE(unsigned char * original, unsigned char * predicted, int puWidth, int puHeight);
#endif
#ifdef __EPI_RISCV
	static int SAD_EPIRISCV(unsigned char * original, unsigned char * predicted, int puWidth, int puHeight);
#endif

	static void FastHaddamardColumn4(int * block, int * transformed, int blockSize, int index);
	static void FastHaddamardColumn8(int * block, int * transformed, int blockSize, int index);
	static void FastHaddamardColumn16(int * block, int * transformed, int blockSize, int index);
	static void FastHaddamardColumn32(int * block, int * transformed, int blockSize, int index);


	static int CalculateBlockMatchingValue(unsigned char * block_1, unsigned char * block_2, int puWidth, int puHeigh, EncodingContext * enCtx);

	static void SubResidual(unsigned char * rawBlock, unsigned char * predictedBlock, int16_t * residualBlock, int blockSize);
	static void SubResidualWithTransformTree(unsigned char* rawBlock, unsigned char* predictedBlock, int blockSize, TU *parentTU, int colorIdx);
	static void AddResidual(int16_t * residualBlock, unsigned char * predictedBlock, unsigned char * reconstructedBlock, int blockSize, int resultBlockSize, int idx);

	static int clip3(const int a, const int b, int x);
	static int sign(const int a);
	static int logarithm2(unsigned int a);
	static int powerOf8(int n);
	static float calculatePSNR(unsigned char* original, unsigned char* reconstructed, int size, int bitNumber);
	static float calculateSSIM(unsigned char * original, unsigned char * reconstructed, int width, int height, int bitNumber);
	static void calculatePSNR_YUV(unsigned char* original, unsigned char* reconstructed, int frameWidth, int frameHeight, int bitNumber, float &psnr_y, float &psnr_u, float &psnr_v);
	static void calculateSSIM_YUV(unsigned char* original, unsigned char* reconstructed, int frameWidth, int frameHeight, int bitNumber, float &ssim_y, float &ssim_u, float &ssim_v);
	static bool doesFileExist(std::string filename);
	static void printArrayOfInt16(int16_t * array, int rowSize, int columnSize);
private:
	//static class
	ComUtil();


};
