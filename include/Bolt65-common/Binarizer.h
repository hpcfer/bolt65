/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "EntropyModel.h"
#include <algorithm>
#include <cmath>
#include "SyntaxElementInfo.h"
#include "ContextMemory.h"

class Binarizer
{
public:

	Binarizer();
	~Binarizer();
	static Bins binarizeElement(syntaxElemEnum elementId, int elementValue, SyntaxElementInfo *elementInfo, ContextMemory *ctxMem);
	static int precalculateValue(syntaxElemEnum elementId, int elementValue);
	static int precalculateParameter(syntaxElemEnum elementId, int elementValue, ContextMemory *ctxMem);
	static void refreshContext(syntaxElemEnum elementId, int elementValue, ContextMemory *ctxMem);
	static Bins bin_special(syntaxElemEnum elementId, int elementValue, ContextMemory * ctxMem);
	static Bins bin_TrU(unsigned int N, unsigned int cMax);
	static Bins bin_TRk(unsigned int N, unsigned int cRiceParam, unsigned int cMax);
	static Bins bin_EGk(unsigned int N, unsigned int k);
	static Bins bin_EGk_lim(unsigned int N, unsigned int riceParam, unsigned int cIdx, int BitDepthY, int BitDepthC);
	static Bins bin_FL(unsigned int N, unsigned int cMax);

	static Bins bin_part_mode(int part_mode, bool CuPredMode, int cbSize, int MinCbLog2SizeY, bool amp_enabled_flag);
	static Bins bin_intra_chroma_pred_mode(int intra_chroma_pred_mode);
	static Bins bin_inter_pred_idc(int inter_pred_idc, int nPbW, int nPbH);
	static Bins bin_cu_qp_delta_abs(int cu_qp_delta_abs);
	static Bins bin_coeff_abs_level_remaining(int coeff_abs_level_remaining, int baseLevel, int cIdx,
		bool persistent_rice_adaptation_enabled_flag, bool transform_skip_flag, bool cu_transquant_bypass_flag,
		bool extended_precision_processing_flag, bool first_time_invoke, unsigned short *StatCoeff,
		unsigned short& cLastAbsLevel, unsigned short& cLastRiceParam, unsigned short bitDepthYVar, unsigned short bitDepthCVar);

	static Bins put(bool val, Bins b);
	static Bins put(Bins addB, Bins b);
	static bool get(Bins& b);

#pragma region "Decoder Part"

#pragma region "Standard Binarization Methods"

	/// <summary> Performs inverse binarization of Truncated Unary binarized bin. </summmary>
	/// <param name="B"> Input bin string value. </param>
	/// <param name="cMax"> Parameter that define maximum value of symbol. Used for truncation process. </param>
	/// <returns> Value of symbol inverse binarization. If the value of the bin string can not be derived from this binarization process, possible parameter will be set to false. <returns>
	static Value invBin_TrU(Bins B, unsigned int cMax);

	/// <summary> Performs inverse binarization of Truncated Rice binarized bin. TRk bin string is a concatenation of prefix bin string and, when present, a suffix bin string. </summary>
	/// <param name="B"> Input bin string value. </param>
	/// <param name="cRiceParam"> Parameter that define size of prefix and suffix bin string. </param>
	/// <param namne="cMax"> Parameter that define maximum value of symbol. Used for truncation process. </param>
	/// <returns> Value of symbol inverse binarization. If the value of the bin string can not be derived from this binarization process, possible parameter will be set to false. </returns>
	static Value invBin_TRk(Bins B, unsigned int cRiceParam, unsigned int cMax);

	/// <summary> Performs inverse binarization of Exp-Golomb binarized bin. Each codeword consist of a unary prefix and suffix. </summary>
	/// <param name="B"> Input bin string value </param>
	/// <param name="k"> Defines order of the Exp-Golomb binarization process. </param>
	/// <returns> Value of symbol inverse binarization. If the value of the bin string can not be derived from this binarization process, possible parametar will be set to false. </returns>
	static Value invBin_EGk(Bins B, unsigned int k);

	// TODO: Define and implement
	static Value invBin_EGk_lim(Bins B, unsigned int riceParam, unsigned int cIdx, int BitDepthY, int BitDepthC);

	/// <summary> Performs inverse binarization of Fixed-Length binarized bin. </summary>
	/// <param name="B"> Input bin string value </param>
	/// <param name="cMax"> Parameter that define maximum value of symbol </param>
	/// <returns> Value of symbol inverse binarized. If the value of the bin string can not be derived from this binarization process, possible parametar wil be set to false. </returns>
	static Value invBin_FL(Bins B, unsigned int cMax);
#pragma endregion

#pragma region "Binarization Methods for Special Cases"

	/// <summary> Performs inverse binarization for part_mode binarized value. The process is specified in Table 9-40 in official standard. </summary>
	/// <param name="part_mode"> Binarized symbol value. </param>
	/// <param name="CuPredMode"> Prediction mode of corresponding CU. If <c>true</c> then is MODE_INTRA else MODE_INTER. </param>
	/// <param name="cbSize"> Luma coding block size of corresponding CU. </param>
	/// <param name="MinCbLog2SizeY"> Minimum size of luma coding block. Defined in syntax element log2_min_luma_coding_block_size_minus3. </param>
	/// <param name="amp_enabled_flag"> Enables asymetric motion partitions (only MODE_INTER). </param>
	/// <returns> Value of symbol inverse binarized. </returns>
	static Value invBin_part_mode(Bins part_mode, bool CuPredMode, int cbSize, int MinCbLog2SizeY, bool amp_enabled_flag);

	/// <summary> Performs inverse binarization for intra_chroma_pred_mode binarized value. The process is specified in Table 9-41 in official standard. </summary>
	/// <param name="intra_chroma_pred_mode"> Binarized symbol value. </param>
	/// <returns> Value of symbol inverse binarized. </returns>
	static Value invBin_intra_chroma_pred_mode(Bins intra_chroma_pred_mode);

	/// <summary> Performs inverse binarization for inter_pred_idc binarized value. The process is specified in Table 9-42 in official standard. </summary>
	/// <param name="inter_pred_idc"> Binarized symbol value. </param>
	/// <param name="nPbW"> Value of the current luma prediction block width. </param>
	/// <param name="nPbH"> Value of the current luma prediction block height. </param>
	/// <returns> Value of symbol inverse binarized. </returns>
	static Value invBin_inter_pred_idc(Bins inter_pred_idc, int nPbW, int nPbH);

	/// <summary> Performs inverse binarization for cu_qp_delta_abs binarized value. This bin string is concatenation of TR binarized prefix and EGk binarized suffix. </summary>
	/// <param name="cu_qp_delta_abs"> Binarized symbol value. </param>
	/// <returns> Value of symbol inverse binarized. </returns>
	static Value invBin_cu_qp_delta_abs(Bins cu_qp_delta_abs);

	/// <summary> Performs inverse binarization for coeff_abs_level_remaining binarized value. </summary>
	/// <param name="coeff_abs_level_remaining"> Binarized symbol value. </param>
	/// <param name="baseLevel"> Value of the coefficient after checking greater than 1 and greater than 2 flag. </param>
	/// <param name="cIdx"> Color index of the corresponding residual block. </param>
	/// <param name="persistant_rice_adaptation_enabled_flag"> If the value of this parameter is <c>true</c>, rice parameter is initialized at the start of each sub-block using mode dependent statistic accumulated from previous sub-blocks. Value derives from syntax element. </param>
	/// <param name="transform_skip_flag"> Flag that specifies whether a transform is applied to the associated transform block. </param>
	/// <param name="cu_transquant_bypass_flag"> If the value of this parameter is <c>true</c>, scaling, transform and in-loop filter processes are bypassed. Value is derived from syntax element. </param>
	/// <param name="extended_precision_processing_flag"> Equal to 1 specifies that an extended dynamic range is used for coefficient parsing and inverse transform processing. </param>
	/// <param name="first_time_invoke"> Must be set <c>true</c> if the inverse binarization process is invoked for the first time for the current sub-block scan index i. </param>
	/// <param name="StatCoeff"> Rice parameter initialization states. </param>
	/// <param name="cLastAbsLevel"> Value derived from cAbsLevel from the last invocation of this inverse binarization process. </param>
	/// <param name="cLastRiceParam"> Value derived from cRiceParam from the last invocation of this inverse binarization process. </param>
	/// <param name="bitDepthYVar"> Value of luma sample bit depth. </param>
	/// <param name="bitDepthCVAr"> Value of chroma samples bit depth. </param>
	/// <returns> Value of symbol inverse binarized. </returns>
	static Value invBin_coeff_abs_level_remaining(Bins coeff_abs_level_remaining, int baseLevel, int cIdx,
		bool persistent_rice_adaptation_enabled_flag, bool transform_skip_flag, bool cu_transquant_bypass_flag,
		bool extended_precision_processing_flag, bool first_time_invoke, unsigned short *StatCoeff, unsigned short& cLastAbsLevel, unsigned short& cLastRiceParam,
		unsigned short bitDepthYVar, unsigned short bitDepthCVar);
#pragma endregion

#pragma endregion


	
};