/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __ARM_NEON
#pragma once
#include "ITransformAndQuantization.h"
#include <arm_neon.h>

using namespace std;

class TransformAndQuantizationNEON : public ITransformAndQuantization
{
public:
	TransformAndQuantizationNEON();
	virtual void quantization(int16_t * A, int QP, int NB, int N, bool * is_zero_matrix, int16_t * C);
	virtual void dequantization(int16_t * A, int QP, int NB, int N, int16_t * C);
	virtual void multiply(int16_t *A, int16_t *B, int N, int16_t *C, int bdShift);
private:

};
#endif