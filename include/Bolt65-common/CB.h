/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "Enums.h"
#include <cstdint>

class CB
{
public:

	int startingIndex;
	int startingIndexInFrame;
	int blockSize;
	int cbQP;

	CB(int starting_index, int block_size);
	CB(int starting_index, int block_size, int lumaQp);
	CB(int starting_index, int block_size, int lumaQp, ColorIdx colorIdx);
	CB(int starting_index, int starting_index_in_frame, int block_size, int lumaQp, ColorIdx colorIdx);
	CB();
	~CB();
};

static const int QPc[52] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,29,30,31,32,33,33,34,34,35,35,36,36,37,37,38,39,40,41,42,43,44,45 };

