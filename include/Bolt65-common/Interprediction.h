/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "ComUtil.h"
#include "Interpolation.h"
#include "Enums.h"
#include <iostream>
#include "EncodingContext.h"
#include "Frame.h"
#include "DPBManager.h"

using namespace std;

class Interprediction
{
private:
	/*Search algorithms*/
	static void ThreeStepSearch(unsigned char * referenceFrame, unsigned char * block, int cuBlockSize, int puIndex, int puWidth, int puHeight, int startingIndex, int areaSize, int & deltaX, int & deltaY, EncodingContext * enCtx, int & chosenBmcValue);
	static void FullIntegerAreaSearch(unsigned char * referenceFrame, unsigned char * block, int cuBlockSize, int puIndex, int puWidth, int puHeight, int startingIndex, int areaSize, int & deltaX, int & deltaY, EncodingContext * enCtx, int & chosenBmcValue);
	static void FullIntegerSearch(unsigned char * referenceFrame, unsigned char * block, int cuBlockSize, int puIndex, int puWidth, int puHeight, int startingIndex, int areaSize, int & deltaX, int & deltaY, EncodingContext * enCtx, int & chosenBmcValue);
	static void FullAreaSearch(unsigned char * referenceFrame, unsigned char * block, int cuBlockSize, int puIndex, int puWidth, int puHeight, int startingIndex, int areaSize, int & deltaX, int & deltaY, EncodingContext * enCtx, int & chosenBmcValue);
	/*END Search algorithms*/

	static void SortCandModeList(int * candModeList);
	static void DeriveCandidatesForAMVP(int * MvpX, int * MvpY, int * MvpT, PU * pu, Partition *partition, int ctbSize, bool useTemporalCandidates);
	static void DeriveCandidatesForMerge(int * MergeCandListX, int * MergeCandListY, int * MergeCandListT, CU * cu, PU * pu, Partition *partition, int ctbSize, bool useTemporalCandidates);
	static void ScaleCandidate(int deltaX, int deltaY, int tb, int td, int &scaledDeltaX, int &scaledDeltaY);
	static void encodeAMVP(PU * pu, Partition *partition, int ctbSize, bool useTemporalCandidates);
	static void encodeMerge(CU * cu, PU * pu, Partition *partition, int ctbSize, bool useTemporalCandidates);

	static void SearchAllFractions(unsigned char * referenceFrame, unsigned char * block,  int predictionStartingIndex, int startingIndex, int puWidth, int puHeight, int &bestBmcValue, int &deltaX, int &deltaY, EncodingContext *enCtx);

public:
	static void EstimateMotion(Frame* referenceFrame, unsigned char* block, int cuBlockSize, PU *pu, EncodingContext *enCtx, int &chosenBmcValue);
	static void FetchBlockFromReferenceFrame(unsigned char * data, int frameWidth, unsigned char * predictedBlock, int startingIndex, int blockWidth, int blockHeight);
	static void ReuseDataWeightedMVs(PU * Pu, float widthCoeff, float heightCoeff);
	static void ReuseMajorMVs(PU * pu, float widthCoeff, float heightCoeff);

	static int FindBestPredictionInter(CU * cu, Partition *partition, DPBManager *dpbm, EncodingContext *enCtx);
	static void GenerateInterPredictedBlock(unsigned char* referenceReconstructedPayload, unsigned char* predictedBlock, int frameWidth, int frameHeight, int puWidth, int puHeight, int puOffset, int originalStartingIndex, int deltaX, int deltaY, int cuBlocksize, int width, ColorIdx colorIdx);
	static void encodeInterPredDecision(CU * cu, Partition *partition, int ctbSize, bool useTemporalCandidates);


	Interprediction();
	~Interprediction();
};

