/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "TBSubblock.h"
#include "Enums.h"
#include <stdint.h>

class TB
{
public:
	bool isZeroMatrix;
	bool transformSkipFlag;
	int scanType;

	int transformBlockSize;
	int16_t *QTResidual;
	int16_t *residual;
	int numOfSubblocks;
	TBSubblock* subBlocks;

	int lastSigCoeffX;
	int lastSigCoeffY;
	int indexOfSigSubblock;

	//This is information forwarder from above, needed for coding
	int colorIdx;

	int intraPredMode;

	int startingIndexInFrame;
	int startingIndex;

	TB();
	TB(int startingTransformIndex, int blockSize);
	TB(int startingTransformIndex, int blockSize, int colorIdx);
	TB(int startingIndex, int startingIndexFrame, int blockSize, int colorIdx);
	~TB();

private:
};

