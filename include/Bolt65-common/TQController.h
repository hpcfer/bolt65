/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "EncodingContext.h"
#include "ITransformAndQuantization.h"
#include "TransformAndQuantizationCreator.h"
#include "CU.h"
#include <cstdint>

class TQController
{
public:
	TQController();
	~TQController();
	void Init(EncodingContext * _enCtx);
	void PerformTransformAndQuantization(int qp, int16_t * blockResidual, unsigned char blockSize, bool * isZeroMatrix, int16_t * tqResidual, int colorIdx, PredMode cuPredMode);
	void PerformInverseTransformAndQuantization(int qp, int16_t * quantizedTransformedResidual, unsigned char blockSize, int16_t * residual, int colorIdx, PredMode cuPredMode);

private:
	EncodingContext * enCtx;
	ITransformAndQuantization *tqKernel;
};

