/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "ComUtil.h"
#include "Enums.h"

using namespace std;

class DeblockingFilter
{
public:

	static void DeblockingFilterBlock(unsigned char* blockDataP, unsigned char* blockDataQ, int blockSize, int QP, DeblockinFilterType deblockingType);
	static void DeblockingFilterBlockChroma(unsigned char* blockDataP, unsigned char* blockDataQ, int blockSize, int QP, DeblockinFilterType deblockingType);
	static void NormalFiltering(unsigned char p[4][4], unsigned char q[4][4], int QP);
	static void StrongFiltering(unsigned char p[4][4], unsigned char q[4][4], int QP);
	static void ChromaFiltering(unsigned char p[4][4], unsigned char q[4][4], int QP);
	
	static void getLeftSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4]);
	static void getRightSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4]);
	static void getTopSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4]);
	static void getBottomSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4]);

	static void getFirstFourBoundaryPixelsByRow(unsigned char* data, int blockSize, int row, unsigned char boundary[4]);
	static void getLastFourBoundaryPixelsByRow(unsigned char* data, int blockSize, int row, unsigned char boundary[4]);
	static void getFirstFourBoundaryPixelsByColumn(unsigned char* data, int blockSize, int column, unsigned char boundary[4]);
	static void getLastFourBoundaryPixelsByColumn(unsigned char* data, int blockSize, int column, unsigned char boundary[4]);

	static void setLeftSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4]);
	static void setRightSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4]);
	static void setTopSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4]);
	static void setBottomSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4]);

	static void setFirstFourBoundaryPixelsByRow(unsigned char* data, int blockSize, int row, unsigned char boundary[4]);
	static void setLastFourBoundaryPixelsByRow(unsigned char* data, int blockSize, int row, unsigned char boundary[4]);
	static void setFirstFourBoundaryPixelsByColumn(unsigned char* data, int blockSize, int column, unsigned char boundary[4]);
	static void setLastFourBoundaryPixelsByColumn(unsigned char* data, int blockSize, int column, unsigned char boundary[4]);

	DeblockingFilter();
	~DeblockingFilter();

};

static const int Beta[54] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,7,8,9,10,11,12,13,14,15,16,17,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64,-1,-1 };
static const int Tc[54] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,5,5,6,6,7,8,9,10,11,13,14,16,18,20,22,24 };

