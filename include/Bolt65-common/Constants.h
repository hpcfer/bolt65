/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include <string>

class StringConstants
{
public:
	static const std::string InputFile;
	static const std::string OutputFile;
	static const std::string NumberOfFrames;
	static const std::string QP;
	static const std::string BitNumber;
	static const std::string HighestTid;
	static const std::string CtbLog2SizeY;
	static const std::string MinCbLog2SizeY;
	static const std::string MinTbLog2SizeY;
	static const std::string MaxTbLog2SizeY;
	static const std::string InputPictureWidth;
	static const std::string InputPictureHeight;
	static const std::string FrameRate;
	static const std::string DPBSize;
	static const std::string GOP;
	static const std::string AMVPUseTemporalCandidates;
	static const std::string DeblockingFilter;
	static const std::string SAOFilter;
	static const std::string Log2MaxPicOrderLst;
	static const std::string SearchAlgorithm;
	static const std::string SearchArea;
	static const std::string CalculatePSNR;
	static const std::string CalculateSSIM;
	static const std::string CalculateBitsPerFrame;
	static const std::string CalculateProcessingTime;
	static const std::string StatMode;
	static const std::string UsePolicy;
	static const std::string PolicySocketHost;
	static const std::string PolicySocketPortNumber;
	static const std::string InterpolateAlgorithm;
	static const std::string BlockMatching;
	static const std::string CSV;
	static const std::string CSVTiles;
	static const std::string AVX;
	static const std::string NEON;
	static const std::string SVE;
	static const std::string SVE2;
	static const std::string EPI_RISCV;
	static const std::string EPI_RISCV_AUTO;
	static const std::string MANGO_HW;
	static const std::string MANGO_NUP;
	static const std::string MANGO_PEAK;
	static const std::string MANGO_GN;
	static const std::string MANGO_GPU;
	static const std::string Threads;
	static const std::string TilesEnabled;
	static const std::string TilesInRow;
	static const std::string TilesInColumn;
	static const std::string ShowStatisticsPerFrame;
	static const std::string ShowStatisticsPerTile;
	static const std::string DynamicTiles;
	static const std::string TileLoadBalancingAlgorithm;
	static const std::string TileLoadBalancingInterval;
	static const std::string InputBufferSize;
	static const std::string OutputBufferSize;
	static const std::string ReuseDecodedData;
	static const std::string TestAllModes;
	static const std::string InputFrameRate;
	static const std::string SMPEnabled;
	static const std::string AMPEnabled;
	static const std::string IntraTUEnabled;

	static const std::string ReuseIntra;
	static const std::string ReuseInter;
	static const std::string ReuseInterpolation;
	static const std::string ReusePartmode;
	static const std::string ReuseMode;
	
	static const char FRAME_I;
	static const char FRAME_P;
	static const char FRAME_B;
};