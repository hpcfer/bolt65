/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "DeblockingFilter.h"
#include "BlockPartition.h"
#include "Partition.h"

class InLoopFilterController
{
public:

	EncodingContext *enCtx;

	void PerformDeblockingFilter(Partition * Partition);

	
	InLoopFilterController();
	~InLoopFilterController();

	void Init(EncodingContext * _enCtx);

private:
	void PerformVerticalDeblockingFilterForPartition(Partition * Partition);
	void PerformHorizontalDeblockingFilterForPartition(Partition * Partition);
	void PerformVerticalDeblockingFilterForCU(Partition * Partition, CU * cu);
	void PerformHorizontalDeblockingFilterForCU(Partition * Partition, CU * cu);
	void PerformDeblockingFilterForBlock(Partition* Partition, CB * cbP, CB *cbQ, DeblockinFilterType type);
	void PerformDeblockingFilterForChroma(Partition* Partition, CB * cbP, CB *cbQ, DeblockinFilterType type);
	int CalculateBoundaryStrength(CU * cuP, CU * cuQ);

};

