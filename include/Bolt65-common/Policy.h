/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "Config.h"
#include "EncodingContext.h"
#include "Statistics.h"

class Policy
{
public:
	
	typedef struct TRANSCODING_PARAMS {

		int resolution_w;
		int resolution_h;
		int frame_r;
	} params_st;

	typedef struct TRANSCODING_CONFIG {
		int QP;
		int CU_size;
		int GOP;
		int SR;
		int searchAlgorithm;

	} config_st;

	typedef struct TRANSCODING_PERFORMANCE {

		float PSNRY;
		float PSNRU;
		float PSNRV;
		long long bitrate;
		float timePerFrame;

	} performance_st;

	static void initializePolicy(Config *configuration, EncodingContext *enCtx);
	static void getOptimumConfiguration(Config *configuration, EncodingContext *enCtx);
	static void changeConfiguration(config_st newConfiguration, Config *configuration, EncodingContext *enCtx);
};