/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once


#include "Config.h"
#include "EncodingContext.h"
#include "Constants.h"
#include "Enums.h"

#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>


class ConfigurationManager
{
public:

	void fetchConfigurationFromFile(Config* config);
	void assignConfigAttribute(string configName, string configValue, Config* config);
	void initializeEncodingContext(EncodingContext * enCtx, Config * config);
	void initializeDecodingContext(EncodingContext * decCtx, Config * config);
	void initializeTranscodingContexts(EncodingContext *enCtx, EncodingContext *decCtx, Config *config);
	void validateGOPStructure(string GOP);
	void validateConfiguration(EncodingContext* enCtx);
	ConfigurationManager();
	~ConfigurationManager();
};


