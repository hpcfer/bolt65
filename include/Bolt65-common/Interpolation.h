/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "ComUtil.h"
#include "BlockPartition.h"
#include <iostream>
#include <string>

class Interpolation
{
public:
	Interpolation();
	static void Interpolate(int nPbH, int nPbW, unsigned char * referencePicture, unsigned char * interpolatedArray, int startingIndex, int frameWidth, int frameHeight, int cIdx, int absdX, int absdY, bool isdXNeg, bool isdYNeg);
	static void Interpolate(int nPbH, int nPbW, unsigned char * referencePicture, unsigned char * interpolatedArray, int startingIndex, int frameWidth, int frameHeight, int cIdx, int absdX, int absdY, bool isdXNeg, bool isdYNeg, int puOffset, int cuBlockSize);
	static void InterpolateNew(int nPbH, int nPbW, unsigned char * referencePicture, unsigned char * interpolatedArray, int startingIndex, int frameWidth, int frameHeight, int cIdx, int dX, int dY);
	static void InterpolateSF5(int nPbH, int nPbW, unsigned char * referencePicture, unsigned char* interpolatedArray, int startingIndex, int frameWidth, int frameHeight, int absdX, int absdY, bool isdXNeg, bool isdYNeg);
	static void AVCStandardInterpolate(int nPbH, int nPbW, unsigned char * referencePicture, unsigned char* interpolatedArray, int startingIndex, int frameWidth, int frameHeight, int absdX, int absdY, bool isdXNeg, bool isdYNeg);
	static void InterpolateBilinear(int nPbH, int nPbW, unsigned char * referencePicture, unsigned char* interpolatedArray, int startingIndex, int frameWidth, int frameHeight, int absdX, int absdY, bool isdXNeg, bool isdYNeg);
	static void InterpolateFourTap(int nPbH, int nPbW, unsigned char * referencePicture, unsigned char* interpolatedArray, int startingIndex, int frameWidth, int frameHeight, int absdX, int absdY, bool isdXNeg, bool isdYNeg);
	~Interpolation();
};

