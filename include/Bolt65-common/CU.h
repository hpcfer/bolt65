/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once

#include "CB.h"
#include "TU.h"
#include "PU.h"
#include "Enums.h"
#include <vector>
#include <utility>
#include <string>

class CU
{
public:
	static int idClass;
	int id;

	std::vector<std::pair<RatioCU, CU*>> transMappedCUs;
	
	int frameWidth = 0;
	int frameHeight = 0;
	int partitionWidth = 0;
	int partitionHeight = 0;
	//props
	bool isSplit;
	bool isLastInRow;
	bool isLastInColumn;
	bool isInFrame;
	bool requiresFurtherSplitting;
	int depth;
	int index;
	int indexInFrame;

	CU* parent;
	bool hasParent;
	CU* children[4];
	bool hasChildren;

	CU* ctuParent;

	CB cbY;
	CB cbU;
	CB cbV;

	CU** ctuTree;


	TU *transformTree;
	bool hasTransformTree;

	int numOfPUs;
	PU *predictionUnits;

	int IntraPredModeY[4];
	int IntraPredModeChroma;
	bool CuTransquantByPassFlag; //not present
	bool CuSkipFlag; //not present
	bool PredModeFlag; //not present
	PredMode CuPredMode = UNKNOWN;
	int PartMode; // we don't have any CU to PU partitioning

	//PartitionMode PMode; //derived from partMode
	bool PcmFlag; //not present
	bool PrevIntraLumaPredFlag[4];
	int MpmIdx[4];
	int RemIntraLumaPredMode[4];
	int IntraChromaPredMode;
	//bool RqtRootCbf; //not present (inferred to be 1)
	bool SplitCuFlag;

	int motionEstimations[4]{ 0 };
	int transformAndQuantizations[4]{ 0 };
	int blocks[4]{ 0 };

	int transformCost = 0;
	int predictionCost = 0;
	int blockCost = 0;

	int numberOfBits = 0;

	

	//methods

	std::string getCUPathID();
	void PerformSplit(int frameWidth, int frameHeight);
	void PerformSplit(int width, int height, int frameWidth, int frameHeight);
	void PerformSplit(int frameWidth, int frameHeight, int minCbLog2SizeY);
	void PerformSplit(int width, int height, int frameWidth, int frameHeight, int minCbLog2SizeY);
	void PerformRecursiveSplit(int minCbLog2SizeY, bool reuseData, CU * cuReuse, float widthCoeff, float heightCoeff);
	void InitializeTransformTree(int lumaSize, int frameWidth, int frameHeight);
	void InitializeTransformTree(int lumaSize);
	void InitializeTransformTree();
	void CreatePredictionUnits(int frameWidth);
	void MergePredictionUnits(int frameWidth);
	void ClearPredictionUnits();
	void setFlagsForFurtherSplitting(int frameWidth, int frameHeight);

	bool isLeftCodingUnitAvailable(int frameWidth);
	bool isAboveCodingUnitAvailable(int frameWidth);

	void RejectPredictionMode();

	CU * getLeftCodingUnit();

	CU * getAboveCodingUnit();

	int getIntraSplitFlag();
	bool getPartOfInterSplitFlag();

	void decodeIntraPredDecision(int width);
	void encodeIntraPredDecision(int width);
	void DeriveCandidatesForIntraPrediction(int * candModeList, int width);
	int getMajorCUFromTransmappedCUs();
	void DeriveCandidatesForIntraPredictionPARTNxN(int * candModeList, int part, int width);
	void SortCandModeList(int * candModeList);
	void getIntraPredictionModeChroma();

	void MapPartModeToPU(float widthCoeff, float heightCoeff);
	void MapToChildren(CU * transcodedCU, float widthCoeff, float heightCoeff);
	void MapOriginalCUToTranscodedCU(CU * transcodedCU, CU * originalCU, int columnStart, int columnEnd, int rowStart, int rowEnd);
	void MapPUtoOriginalPU(PU * predictionUnit, PU * originalPU, int columnStart, int columnEnd, int rowStart, int rowEnd, int originalFrameWidth);
	void MapIntraToOriginalIntra(float widthCoeff, float heightCoeff);

	void MapOriginalCUToTranscodedCU(CU * transcodedCU, CU * originalCU);
	void mapAllAsWhole(CU * transcodedCU, CU * originalCU);
	void sort();

	CU();
	CU(int frameWidth, int frameHeight, int partitionWidth, int partitionHeight);
	CU(int _frameWidth, int _frameHeight, int _partitionWidth, int _partitionHeight, CB _cbY, CB _cbU, CB _cbV);
	~CU();

private:

	CU * leftCU;
	CU *aboveCU;
	int majorMappedCUIndex;
	bool isMajorMappedCUCached = false;
	bool isLeftAvailable;
	bool isAboveAvailable;
	int IntraSplitFlag;
	bool isLeftCached = false;
	bool isAboveCached = false;


};


