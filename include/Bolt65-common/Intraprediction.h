/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "CU.h"
#include "BlockPartition.h"
#include "ComUtil.h"
#include "Partition.h"

#include <iostream>
#include <algorithm>

class Intraprediction
{
public:
	Intraprediction();
	~Intraprediction();
	static void RefSub32x32(unsigned char * refSamples, bool * refSamplesExists);
	static void RefSub16x16(unsigned char * refSamples, bool * refSamplesExists);
	static void RefSub8x8(unsigned char * refSamples, bool * refSamplesExists);
	static void RefSub4x4(unsigned char * refSamples, bool * refSamplesExists);
	static void ThreeTapRefFilter32x32(unsigned char * refSamples, unsigned char * filteredRefSamples);
	static void ThreeTapRefFilter16x16(unsigned char * refSamples, unsigned char * filteredRefSamples);
	static void ThreeTapRefFilter8x8(unsigned char * refSamples, unsigned char * filteredRefSamples);
	static void ThreeTapRefFilter4x4(unsigned char * refSamples, unsigned char * filteredRefSamples);
	static void LinearInterpolation32x32(unsigned char* refSamples, unsigned char* filteredRefSamples);
	static void ReferenceRowExtensionForNegativePrediction(unsigned char* refSamples, unsigned char predMode, unsigned char blockSize);
	static void AngularPrediction32x32(unsigned char * refSamples, unsigned char predMode, unsigned char * predictedBlock);
	static void AngularPrediction16x16(unsigned char * refSamples, unsigned char predMode, unsigned char * predictedBlock, int cIdx);
	static void AngularPrediction8x8(unsigned char * refSamples, unsigned char predMode, unsigned char * predictedBlock, int cIdx);
	static void AngularPrediction4x4(unsigned char * refSamples, unsigned char predMode, unsigned char * predictedBlock, int cIdx);
	static void DCPrediction32x32(unsigned char * refSamples, unsigned char * predictedBlock);
	static void DCPrediction16x16(unsigned char * refSamples, unsigned char * predictedBlock, int cIdx);
	static void DCPrediction8x8(unsigned char * refSamples, unsigned char * predictedBlock, int cIdx);
	static void DCPrediction4x4(unsigned char * refSamples, unsigned char * predictedBlock, int cIdx);
	static void PlanarPrediction32x32(unsigned char * refSamples, unsigned char * predictedBlock);
	static void PlanarPrediction16x16(unsigned char * refSamples, unsigned char * predictedBlock);
	static void PlanarPrediction8x8(unsigned char * refSamples, unsigned char * predictedBlock);
	static void PlanarPrediction4x4(unsigned char * refSamples, unsigned char * predictedBlock);

	static void DeriveCandidatesForIntraPrediction(int* candModeList, CU *cu, int partitionWidth);
	static void DeriveCandidatesForIntraPredictionPARTNxN(int * candModeList, CU * cu, int part, int partitionWidth);

	//TODO Only two functions below should be public...This currently works on the encoder, but the decoder's PredictionController is not yet refactored.
//public:
	static void FindBestPredictionIntra(CU * cu, Partition *partition, EncodingContext *enCtx);
	static void GenerateIntraPredictedBlock(int predModeIntra, unsigned char * refSamples, bool * refSamplesExist, unsigned char * predictedBlock, int cIdx, int blockSize);
	
};
static short AngularIntrapredictionModes[33][2] = {
	{32, 0}, //mode 2, index 0
	{26, 0},
	{21, 0},
	{17, 0},
	{13, 0},
	{9, 0},
	{5, 0},
	{2, 0},
	{0, 0},
	{-2, -4096},
	{-5, -1638},
	{-9, -910},
	{-13, -630},
	{-17, -482},
	{-21, -390},
	{-26, -315},
	{-32, -256},
	{-26, -315},
	{-21, -390},
	{-17, -482},
	{-13, -630},
	{-9, -910},
	{-5, -1638},
	{-2, -4096},
	{0,0},
	{2,0},
	{5,0},
	{9,0},
	{13,0},
	{17,0},
	{21,0},
	{26,0},
	{32,0} //mode 34
};


