/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "Enums.h"

class EncodingContext
{
public:
	EncodingContext();
	void clearEncodingContext();
	~EncodingContext();

	NALType nalType;

	int width;
	int height;
	int frameRate;
	int numOfFrames;
	int maxCUsize;
	int ctbSize;
	int ctbLog2SizeY;
	int maxTbLog2SizeY;
	int minCbLog2SizeY;
	int minTbLog2SizeY;

	int highestTid;
	
	int quantizationParameter;
	int initQP;
	int bitNumber;

	bool disableDeblockingFilter;
	bool saoEnabled;
	bool useSMP;
	bool useAMP;
	bool useIntraTU;

	int dpbSize;

	bool useTemporalAMVP;

	int log2MaxPicOrderLst;

	int searchAlgorithm;
	int searchArea;
	int interpolationAlgorithm;
	int blockMatching;

	int currentVPSId;
	int currentSPSId;
	int currentPPSId;

	int threads;

	bool AVX;
	bool NEON;
	bool SVE;
	bool SVE2;
	bool EPI_RISCV;
	bool EPI_RISCV_AUTO;
	bool MANGO_HW;
	bool MANGO_PEAK;
	bool MANGO_GN;
	bool MANGO_GPU;
	bool MANGO_NUP;

	bool tilesEnabled;
	bool isUniform;
	int numberOfTilesInRow;
	int numberOfTilesInColumn;
	int* rowHeights;
	int* columnWidths;
	int* tileOffsets;


	int inputBufSize;
	int outputBufSize;
	int tileLoadBalancingAlgorithm;
	int tileLoadBalancingInterval;

	bool reuseData = false;

	bool testAllModes = false;
	int num_short_term_ref_pic_sets = 0;
	int cabac_init_present_flag = 0;
	bool cabac_init_flag = false;
	int max_transform_hierarchy_depth_inter = 0;
	int strong_intra_smoothing_enabled_flag = 0;
	int initType = 0;
	float tr_width = 0.0;
	float tr_height = 0.0;

	bool reuseIntraMode = false;
	bool reuseInterMode = false;
	bool reuseInterpolation = false;
	bool reusePartMode = false;
	bool reuseMode = false;


	int majorSelected = 0;


		
	//VPS* VPS(var options);
	//SPS* activeactiveSSP;
};

 