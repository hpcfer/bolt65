/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/


#pragma once
#include "ITransformAndQuantization.h"
#include "TransformAndQuantizationCPU.h"
#include "TransformAndQuantizationAVX.h"
#include "TransformAndQuantizationSVE.h"
#include "TransformAndQuantizationNEON.h"
#include "TransformAndQuantizationEPIRISCV.h"
#include "TransformAndQuantizationRISCVAuto.h"
#include "EncodingContext.h"

using namespace std;

class TransformAndQuantizationCreator
{
public:
	~TransformAndQuantizationCreator();
	static ITransformAndQuantization* Create(EncodingContext *enCtx);
private:
	TransformAndQuantizationCreator();

};
