/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include <iostream>  
#include <string>
#include <fstream>
#include <cmath>
#include "CU.h"
#include "EncodingContext.h"


using namespace std;

class BlockPartition
{
public:
	static unsigned char* getBlockBy2DCoordinates(char * data, int blockWidth, int blockHeight, int frameWidth, int frameHeight, int coordinateX, int coordinateY);
	static void get2DCoordinatesFromStartingIndex(int startingIdx, int partitionWidth, int & x, int & y);
	static bool getCandidateBy2DCoordinates(CU ** ctuTree, int pixelRow, int pixelColumn, int partitionWidth, int partitionHeight, int ctbSize, PU &pu);
	static PU * getCandidateBy2DCoordinates(CU ** ctuTree, int pixelRow, int pixelColumn, int partitionWidth, int partitionHeight, int ctbSize, bool & isAvailable);


	static void get1dBlockByStartingIndex(char * data, unsigned char * resultBlock, int startingIndex, int blockWidth, int blockHeight, int frameWidth, int frameHeight);
	static void get1dBlockByStartingIndexWithOffset(char * data, unsigned char * resultBlock, int yOffset, int xOffset, int startingIndex, int cuBlockSize, int frameWidth, int frameHeight, int puWidth, int puHeight, int puOffset);
	
	static void fill1dBlockByStartingIndex(unsigned char* block, int startingIndex, int blockSize, unsigned char * data, bool * isDataValid, int frameWidth, int frameHeight);
	static void getReferenceSamples(int startingIndex, int startingIndexTile, int blockSize, int frameWidth, int frameHeight, int tileWidth, int tileHeight, unsigned char* payload, bool* isValid, unsigned char* refSamples, bool *refSamplesValid);

	static bool isCornerLeft(int startingIndex, int frameWidth);
	static bool isCornerRight(int startingIndex, int frameWidth, int blockSize);
	static bool isCornerUpper(int startingIndex, int frameWidth);
	static bool isCornerBottom(int startingIndex, int blockSize, int frameWidth, int frameHeight);

	static bool isCornerLeftInCTU(CU *cu, int frameWidth);
	static bool isCornerRightInCTU(CU *cu, int frameWidth);
	static bool isCornerUpperInCTU(CU *cu);
	static bool isCornerBottomInCTU(CU *cu, int frameWidth);

	static CU* getParentCTU(CU *cu);

	static void getCUPath(CU *cu, int* indexes, int depth);
	static void getCUPathWithCTU(CU *cu, int* indexes, int depth);

	static CU* getTopRightChild(CU *cu);
	static CU* getTopLeftChild(CU *cu);
	static CU* getBottomLeftChild(CU *cu);
	static CU* getBottomRightChild(CU *cu);

	static CU* getLeftBrotherInSameParent(CU *cu, int index);
	static CU* getAboveBrotherInSameParent(CU *cu, int index);
	static CU* getRightBrotherInSameParent(CU *cu, int index);
	static CU* getBottomBrotherInSameParent(CU *cu, int index);

	static CU* getLeftNeighbourFromRoot(CU *cu, int* indexes, int z);
	static CU* getRightNeighbourFromRoot(CU *cu, int* indexes, int z);
	static CU* getLeftBottomNeighbourFromRoot(CU *cu, int* indexes, int z);
	static CU* getRightBottomNeighbourFromRoot(CU *cu, int* indexes, int z);
	static CU* getAboveNeighbourFromRoot(CU *cu, int* indexes, int z);
	static CU* getAboveRightNeighbourFromRoot(CU *cu, int* indexes, int z);
	static CU* getBottomNeighbourFromRoot(CU *cu, int* indexes, int z);
	static CU* getBottomRightNeighbourFromRoot(CU *cu, int* indexes, int z);

	static CU* getLeftInSameCTU(CU *cu, int* indexes);
	static CU* getRightInSameCTU(CU *cu, int* indexes);
	static CU* getLeftBottomInSameCTU(CU *cu, int* indexes);
	static CU* getRightBottomInSameCTU(CU *cu, int* indexes);
	static CU* getAboveInSameCTU(CU *cu, int* indexes);
	static CU* getAboveRightInSameCTU(CU *cu, int* indexes);
	static CU* getBottomInSameCTU(CU *cu, int* indexes);
	static CU* getBottomRightInSameCTU(CU *cu, int* indexes);

	static CU* getLeftCodingUnit(CU ** ctuTree, CU * current, bool &isAvailable, int frameWidth);
	static CU* getRightCodingUnit(CU ** ctuTree, CU * current, bool &isAvailable, int frameWidth);
	static CU* getAboveCodingUnit(CU ** ctuTree, CU * current, bool &isAvailable, int frameWidth);
	static CU* getBottomCodingUnit(CU ** ctuTree, CU * current, bool &isAvailable, int frameWidth, int frameHeight);
	static CU* getLeftBottomCodingUnit(CU ** ctuTree, CU * current, bool &isAvailable, int frameWidth);
	static CU* getLeftDownCodingUnit(CU ** ctuTree, CU * current, bool &isAvailable, int frameWidth, int frameHeight);
	static CU* getLeftUpCodingUnit(CU ** ctuTree, CU * current, bool &isAvailable, int frameWidth);
	static CU* getBottomRightCodingUnit(CU ** ctuTree, CU * current, bool &isAvailable, int frameWidth, int frameHeight);
	static CU* getRightBottomCodingUnit(CU ** ctuTree, CU * current, bool &isAvailable, int frameWidth);
	static CU* getAboveRightCodingUnit(CU ** ctuTree, CU * current, bool &isAvailable, int frameWidth);
	static CU* getRightUpCodingUnit(CU ** ctuTree, CU * current, bool &isAvailable, int frameWidth);
	static CU* getRightDownCodingUnit(CU ** ctuTree, CU * current, bool &isAvailable, int frameWidth, int frameHeight);

	static CU* getRefCenterCodingUnit(CU**refCtuTree, CU * current);
	static CU* getRefRightDownCodingUnitByIndex(CU ** refCtuTree, CU * current, bool &isAvailable, int frameWidth, int frameHeight);
	static CU* getCodingUnitByLumaPixelIndex(CU ** ctuTree, int pixelIndex, bool &isAvailable, int ctuSize, int frameWidth, int frameHeight);
	static CU* getCodingUnitInCUByLumaPixelIndex(CU * parent, int pixelIndex, int frameWidth);

	static void assignAllNeighbours(CU ** ctuTree, CU * current, int frameWidth);
	static void get1dIntBlockByStartingIndex(char * data, int16_t* resultBlock, int startingIndex, int blockSize, int frameWidth, int frameHeight);
	static void get1dInt8BlockByStartingIndex(int8_t * data, int8_t* resultBlock, int startingIndex, int blockSize, int frameWidth, int frameHeight);
	static void get1dInt16BlockByStartingIndex(int16_t * data, int16_t * resultBlock, int startingIndex, int blockSize, int frameWidth, int frameHeight);
private:
	BlockPartition();
	~BlockPartition();

};
