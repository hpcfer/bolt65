/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once

#include <iostream>
#include <string>
using namespace std;

class Config
{
public:
	string inputFilePath;
	string outputFilePath;
	string version;

	int numberOfFrames;

	int picWidth;
	int picHeight;

	int frameRate;
	int qp;
	int bitNumber;

	bool deblockingFilter;
	bool saoFilter;
	bool useSMP;
	bool useAMP;
	bool useIntraTU;

	int searchAlgorithm;
	int searchArea;
	int dpbSize;
	int interpolationAlgorithm;
	int blockMatching;

	int ctbLog2SizeY;
	int minCbLog2SizeY;
	int minTbLog2SizeY;
	int maxTbLog2SizeY;
	bool AMVPUseTemporalCandidates;
	int log2MaxPicOrderLst;

	int highestTid;

	bool AVX;
	bool NEON;
	bool SVE;
	bool SVE2;
	bool EPI_RISCV;
	bool EPI_RISCV_AUTO;

	bool MANGO_GN;
	bool MANGO_HW;
	bool MANGO_NUP;
	bool MANGO_PEAK;
	bool MANGO_GPU;

	int threads;

	int inputBufSize;
	int outputBufSize;

	bool tilesEnabled;
	bool isUniform;
	int numberOfTilesInRow;
	int numberOfTilesInColumn;
	int* rowHeights;
	int* columnWidths;

	// check if the run is benchmark instead
	bool benchmark;
	string benchmarkFileName;
	
	//define GOP structure 
	//Example GOP:  IBBPBBPBBPBBI   =>  GOP_M=3, GOP_N=12
	//For all intra GOP_M=! and GOP_N=1
	//int GOP_M; // distance between two anchor frames (I or P) not used
	//int GOP_N; // distance between two full images (I-frames) not used
	string GOP;

	bool calculatePSNR;
	bool calculateSSIM;
	bool calculateBits;
	bool calculateTime;
	bool showStatsPerFrame;
	bool showStatsPerTile;
	bool statMode;

	bool outputStatsToFile;
	string outputStatsFileName;

	bool outputStatsTileToFile;
	string outputStatsTileFileName;

	string command;

	bool externalConfig;
	string externalConfigFileName;

	bool usePolicy;
	string policyHost;
	int policyPort;

	bool useDynamicTiles;
	bool tileChanged;
	int tileLoadBalancingAlgorithm;
	int tileLoadBalancingInterval;

	bool reuseData = false;

	bool testAllModes = false;
	int inputFrameRate;

	Config();
	~Config();
	void SetInputFilePath(string _inputFilePath);

	bool reuseIntraMode = false;
	bool reuseInterMode = false;
	bool reuseInterpolation = false;
	bool reusePartMode = false;
	bool reuseMode = false;
};

