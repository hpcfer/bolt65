/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "EntropyBuffer.h"

class AritmeticEncoder
{
public:
	AritmeticEncoder();
	~AritmeticEncoder();

	void initArithmeticCoder(bool isFirstInSliceSegment, EntropyBuffer* _eb);
	void initArithmeticCoder(bool isFirstInSliceSegment);

	void encodeDecision(ProbabilityModel *probabilityModel, bool binVal);
	void encodeBypass(bool binVal);
	void bypassAlignment();
	void encodeTerminate(bool binVal);
	void encodeFlush();
	void byteStuffing(unsigned int rawMinCuBits, unsigned int numBytesInVclNalUnits, unsigned int picSizeInMinCbsY, unsigned int binCountsInNalUnits);
	void renormE();

	void putBit(bool val);

private:
	EntropyBuffer *eb;

	unsigned short ivlLow;
	unsigned short ivlCurrRange;
	bool firstBitFlag;
	unsigned int bitsOutstanding;
	unsigned int binCountsInNalInits;

	Bins arithmeticCode; //unneccesary
};