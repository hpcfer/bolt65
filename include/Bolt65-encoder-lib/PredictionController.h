/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include <thread>
#include "Frame.h"
#include "TQController.h"
#include "BlockPartition.h"
#include "Intraprediction.h"
#include "Interprediction.h"
#include "ComUtil.h"
#include "DPBManager.h"
#include "Constants.h"
#include "ComUtil.h"

#include <condition_variable>
#include <algorithm>

namespace encoder
{
	class PredictionController
	{
	public:
		void Init(TQController * _tQController, DPBManager * _dpbm, EncodingContext * _enCtx);
		void PerformPrediction(Partition *_partition);

		PredictionController();
		~PredictionController();

	private:
		Partition *partition;
		DPBManager *dpbm;
		TQController *tQController;
		EncodingContext* enCtx;

		void PredictBlock(CU * cu, char frameType);

		void GenerateResidualBlockIntra(CU * cu);
		void GenerateResidualBlockInter(CU * cu);
		void GenerateResidualBlockInterIntra(CU * cu);
		void GenerateResidualBlockTransformUnit(TU * tu, int & intraPredModeY, int & intraPredModeChroma, int qpY, int qpU, int qpV, int _depth);

		void ReconstructBlockIntra(CU * cu);
		void ReconstructBlockInter(CU * cu);
		void ReconstructBlockInterIntra(CU * cu);
		void ReconstructBlockIntraTransformUnit(TU * tu, int & intraPredModeY, int & intraPredModeChroma, int qpY, int qpU, int qpV, int _depth);

		void ConstructQTLevelsBlockIntra(CU * cu);
		void ConstructQTLevelsBlockInter(CU * cu);
		void ConstructQTLevelsBlockInterIntra(CU * cu);
		void ConstructQTLevelsBlockIntraTransformUnit(TU * tu, int & intraPredModeY, int & intraPredModeChroma, int qpY, int qpU, int qpV, int _depth);
	};
}
