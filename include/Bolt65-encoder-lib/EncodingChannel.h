/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <iomanip>
#include "Config.h"
#include "EncodingContext.h"
#include "IOController.h"
#include "EntropyController.h"
#include "PartitionProcessor.h"
#include "ConfigurationManager.h"
#include "Enums.h"
#include "DPB.h"
#include "DPBManager.h"
#include "InLoopFilterController.h"
#include "LoadBalancing.h"
#include "Policy.h"
#include "Scanner.h"
#include "Statistics.h"
#include <map>

using namespace encoder;

class EncodingChannel
{
public:
	EncodingChannel();
	~EncodingChannel();

	void StartEncoding(char* argv[], int argc);

	void Configure(char* argv[], int argc);
	void OpenIOStreams();
	void Initialize();
	void Run();


private:
	ENCODER_STATUS STATUS;
	Config configuration;
	EncodingContext enCtx;

	IOController ic;

	ConfigurationManager cc;
	EntropyController ec;
	InLoopFilterController loopc;
	DPBManager dpbm;

	PartitionProcessor **partitionProcessors;

	Statistics stats;

	int numOfPartitions;
	int **tilesPerThread;

	bool addNewVPS = true;
	bool addNewSPS = true;
	bool addNewPPS = true;

	bool checkIsEndOfVideoSequence(int numOfFramesLeft, bool lastFrameProcessed);
	bool checkIsEndOfVideo(int numOfFramesLeft, bool lastFrameProcessed);
	bool checkIfNewPPSIsRequired();
	bool checkIfNewSPSIsRequired();
	bool checkIfNewVPSIsRequired();
	void clearMemory();

	void adaptTiles(int index);
	void reinitializeProcessPartitions(int oldNumberOfPartitions, int newNumberOfPartitions);

	void checkExternalConfig(Config &config, char* argv[], int argc);
	void parseConsoleParameters(Config &config, char* argv[], int argc);

	void ThreadProcessor(int threadId, Frame *frame);
};

