/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once

#include "ioBlock.h"
#include "EncodingContext.h"
#include "Config.h"
#include "Frame.h"
#include "Enums.h"
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable> 

namespace encoder
{
	class IOController
	{
	public:
		string path;
		InputFormatType inputType;
		int nFrames;
		std::queue<unsigned char*> inputBuffer;

		IOController();
		IOController(Config* config);
		~IOController();

		void StartPrefetch(int _nFrames);
		void GetNextFrame(Frame* frame, bool& lastFrameProccessed);
		void PreFetchFrame();
		void InitIO(Config * config, EncodingContext * enCtx);

		void closeFiles();
		void readBits(unsigned short* bitString, unsigned int n);
		int writeBufferToFile(EntropyBuffer *buffer);

	private:

		std::condition_variable inputBufferNotFull;
		std::condition_variable inputBufferNotEmpty;
		std::mutex mx;

		FILE *inputFilePointer;
		ioBlock ioKernel;
		int frameNumber;

		int inputBufSize;
		int outputBufSize;

		std::thread fetchThread;

		void determineInputFileType(string inputFileName);
	};
}


