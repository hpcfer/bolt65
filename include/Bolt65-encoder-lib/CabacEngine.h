/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "EntropyModel.h"
#include "ContextMemory.h"
#include "SyntaxElementInfo.h"
#include "AritmeticEncoder.h"
#include "EntropyBuffer.h"

#include<algorithm>

using namespace std;

class CabacEngine
{
public:

	ContextMemory *ctxMem;
	ProbabilityModelSelection	*probMods = nullptr;
	AritmeticEncoder aritmeticEncoder;
	ctxStruct4ALG1 ctxDataALG1;

	unsigned char ctxSet;							// Made static since its value has to be saved for second and subsequent invocations of the method within sub-block 
	unsigned char greater1Ctx;						// Made static since its value has to be saved for second and subsequent invocations of the method within sub-block
	unsigned char greater1CtxPrevSubBlock = 0;

	unsigned char activeSubBlockScanIdx = 0;
	bool firstTime = true;

	CabacEngine();
	~CabacEngine();

	void CabacInit(ContextMemory * ctMem, int QP, EntropyBuffer * ioC);
	void CabacReset(ContextMemory * ctMem, int QP, EntropyBuffer * ioC);

	void cabacEncode(int binLength, unsigned long binBits, SyntaxElementInfo *info);

	unsigned short derivateSpecCtxIdx(int ctxIncTableIdx, int i);
	unsigned char derivateCtxInc_split_cu_flag(unsigned char cqtDepth, unsigned char ctDepthNbL, unsigned char ctDepthNbA, bool availableL, bool availableA);
	unsigned char derivateCtxInc_cu_skip_flag(bool cu_skip_flagNbL, bool cu_skip_flagNbA, bool availableL, bool availableA);
	unsigned char derivateCtxIncCSBF(unsigned short cIdx, bool csbfRight, bool csbfBottom, unsigned char xS, unsigned char yS, unsigned short log2TrafoSize);
	unsigned char derivateCtxIncLAST(unsigned short binIdx, unsigned short cIdx, unsigned short log2TrafoSize);
	unsigned char derivateCtxIncSIG(unsigned short cIdx, unsigned char xC, unsigned char yC, unsigned char scanIdx, unsigned short log2TrafoSize, bool csbfRight, bool csbfBottom,
		bool transform_skip_context_enabled_flag, bool transform_skip_flag, bool cu_transquant_bypass_flag);
	ctxStruct4ALG1 derivateCtxIncALG1(unsigned short cIdx, unsigned char subblockScanIdx, unsigned char coeffScanIdx, bool lastALG1);
	unsigned char derivateCtxIncALG2(unsigned short cIdx, unsigned char ctxSet);

	void clean();
};