/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "MemoryManager.h"

class Streamer
{
private:

	unsigned char * emulationByte_0 = nullptr;
	unsigned char * emulationByte_1 = nullptr;
	unsigned char * emulationByte_2 = nullptr;
	unsigned char * emulationByte_3 = nullptr;

	unsigned short bitCounter;
	unsigned short emuBuffEmpty;
	
	long address;
	long startAddress;
	long endAddress;
	bool END;

	/// <summary>
	/// Verifies if emulation buffer array with next byte from output buffer equals
	/// start code prefix or zero byte with start code prefix.
	/// </summary>
	/// <returns>
	///	1 - when zero byte and start code prefix upcoming
	/// 2 - when start code prefix upcoming
	/// 3 - when emulation prevetion byte upcoming
	/// 0 - otherwise
	/// </returns>
	int IsStartCodePrefix();

	/// <summary>
	/// Get next byte from bitstream (byte aligned).
	/// </summary>
	/// <returns>Pointer to the next byte from the bitstream.</returns>
	unsigned char * GetNextByte();

public:
	long long paramNumberOfBits;
	/// <summary>
	/// Basic constructor for the streamer object.
	/// </summary>
	Streamer();

	/// <summary>
	/// Constructor that initializes the starting address for the bitstream access.
	/// </summary>
	/// <param name="startingAddress">The starting address of the streamer for addressing in memory manager.</param>
	Streamer(long startingAddress);

	/// <summary>
	/// Gets next bit from the input bitstream.
	/// Handles the emulation prevetion process.
	/// </summary>
	/// <returns>Returns value of the next bit form input bitstream.</returns>
	bool GetBit();

	void GetBits(unsigned short * bitString, unsigned int n);

	void ByteAlign();

	int Release();

	bool IsEND();

	void CleanUp();

	long GetEndAddress();

	void Reset();

	void ReloadBuffers();
};