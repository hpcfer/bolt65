/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once

#include <stack>
#include "ContextMemory.h"
#include "Enums.h"

using namespace std;

class CUContext
{
public:
	CUContext();
	CUContext(CUContext* aboveCU, CUContext* leftCU, CUContext* parent, int ctuDepth, int cuIndex);
	~CUContext();

	int calculateDepthAbove(stack <int> *path, bool downfall);
	int calculateDepthLeft(stack <int> *path, bool downFall);
	int calculateCuSkipLeft(stack <int> *path, bool downfall);
	int calculateCuSkipAbove(stack <int> *path, bool downfall);
	int getPredModeAbove(stack <int> *path, bool downfall);
	int getPredModeLeft(stack <int> *path, bool downfall);
	bool isCUUpperEdgeOfCTU();
	int getIntraPredictionModeLuma(bool PrevIntraLumaPredFlag, int MpmIdx, int RemIntraLumaPredMode, int leftPredMode, int abovePredMode, bool isCuOnUpperEdge);
	int getIntraPredictionModeChroma(int intra_chroma_pred_mode, int IntraPredModeY);

	static void refreshScanIndex(ContextMemory * ctxMem);

	void deleteAllNodes();

	CUContext* aboveCU;
	CUContext* leftCU;

	CUContext** cqt;
	CUContext* parent;

	int cuDepth;
	int index;
	int predMode;
	int cu_skip_flag;
	int intraSplitFlag;
};