/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "EntropyModel.h"
#include "Streamer.h"

class ArithmeticDecoder 
{
public:
	ArithmeticDecoder (Streamer * streamObject);
	ArithmeticDecoder();
	~ArithmeticDecoder ();

	void initArithmeticDecoder();
	void decodeDecision(ProbabilityModel *probabilityModel);
	void renormD();
	void decodeBypass();
	void decodeTerminate();

	bool binVal;

private:
	Streamer * streamObject;

	unsigned short ivlOffset;
	unsigned short ivlCurrRange;


};
