/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once

#include "ioBlock.h"
#include "EncodingContext.h"
#include "Config.h"
#include "Frame.h"
#include "EntropyBuffer.h"
#include "MemoryManager.h"

namespace decoder
{
	class IOController
	{
	public:
		string path;

		IOController();
		~IOController();

		void InitIO(Config * config, EncodingContext * decCtx);
		void writeToOutputFile(char* data, int dataSize);
		void closeFiles();

	private:
		FILE *inputFilePointer;
		ofstream outputFile;

		ioBlock ioKernel;
		int frameNumber;

		int inputBufSize;
		int outputBufSize;

		void determineInputFileType(string inputFileName);

	};
}