/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "Config.h"
#include "EncodingContext.h"
#include "CTBController.h"
#include "TQController.h"
#include "IOController.h"
#include "EntropyController.h"
#include "ConfigurationManager.h"
#include "Enums.h"
#include "DPB.h"
#include "DPBManager.h"
#include "InLoopFilterController.h"
#include "Scanner.h"
#include "PartitionProcessor.h"
#include "Statistics.h"

using namespace decoder;

class DecodingChannel 
{
public:

	DecodingChannel();
	void StartDecoding(char* argv[], int argc);

	void Configure(char* argv[], int argc);
	void OpenIOStreams();
	void Initialize();
	void Run();
	~DecodingChannel();

private:
	DECODER_STATUS STATUS;
	Config configuration;
	EncodingContext decCtx;

	Streamer *streamObject;
	
	PartitionProcessor **partitionProcessors;
	int numOfPartitions;
	int **tilesPerThread;
	bool processesInitialized = false;

	EntropyController decEc;
	ConfigurationManager cc;
	IOController decIc;
	DPBManager dpbm;

	Statistics stats;


	void ThreadProcessor(int threadId, Frame *frame);
	void InitializeProcesses();
	void clearMemory();

	void checkExternalConfig(Config &config, char* argv[], int argc);
	void parseConsoleParameters(Config &config, char* argv[], int argc);

};

