/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "Partition.h"
#include "BlockPartition.h"
#include "Intraprediction.h"
#include "Interprediction.h"
#include "TQController.h"
#include "ComUtil.h"
#include "DPBManager.h"
#include "Constants.h"
#include "ComUtil.h"

namespace decoder
{
	class PredictionController
	{
	public:
		PredictionController();
		~PredictionController();

		void Init(TQController * _decTQQController, DPBManager * _dpbm, EncodingContext * _decCtx);

		void PerformInversePrediction(Partition * _partition);
		void GenerateIntraPredictedBlock(int predModeIntra, unsigned char * refSamples, bool * refSamplesExist, unsigned char * predictedBlock, int cIdx, int blockSize);
		void GenerateInterPredictedBlock(unsigned char * referenceReconstructedPayload, unsigned char * predictedBlock, int puWidth, int puHeight, int puOffset, int originalStartingIndex, int deltaX, int deltaY, int cuBlockSize, int width, ColorIdx colorIdx);

	private:
		Partition *partition;
		DPBManager *dpbm;
		TQController *decTQController;
		EncodingContext* decCtx;

		void InversePredictBlock(CU * cu);
		void InversePredictBlockIntraTransformUnit(TU * tu, int intraPredModeY, int intraPredModeChroma, int qpY, int qpU, int qpV, int _depth);
		void InversePredictBlockInter(CU * cu);
		void DeriveCandidatesForAMVP(int * MvpX, int * MvpY, int * MvpT, PU * pu);
		void DeriveCandidatesForMerge(int * MvpX, int * MvpY, int * MvpT, CU * cu, PU * pu);
		void ScaleCandidate(int deltaX, int deltaY, int tb, int td, int &scaledDeltaX, int &scaledDeltaY);
		void decodeAMVPPredDecision(PU *pu, CU *cu);
		void decodeMergePredDecision(PU *pu, CU * cu);
	};
}