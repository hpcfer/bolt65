/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "CTBController.h"
#include "TQController.h"
#include "PredictionController.h"
#include "Scanner.h"
#include "InLoopFilterController.h"
#include "EntropyController.h"

namespace decoder
{
	class PartitionProcessor
	{
	public:
		Partition *partition;
		EntropyController decEc;

		PartitionProcessor();
		~PartitionProcessor();

		void processPartition();
		void initPartition(Partition * _partition, EncodingContext * decCtx, DPBManager * dpbm, long partitionOffset);

	private:
		CTBController decCtb;
		TQController tq;
		PredictionController decPc;
		InLoopFilterController loopc;
		Streamer *streamObject;
	};
}
