/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once

#include "Enums.h"
#include "SyntaxElementInfo.h"
#include "SyntaxMap.h"
#include "Streamer.h"
#include "EntropyModel.h"
#include "Binarizer.h"
#include "ArithmeticDecoder.h"
#include "Scanner.h"
#include "CUContext.h"
#include "Stringify.h"

#include <queue>
#include <stack>

using namespace std;

/* Global Definitions ---------------------------------------------------------*/
#define bufferSize			1024
#define bitDepthY			8
#define bitDepthC			8
#define RawMinCuBits		768
#define PicSizeInMinCbsY	6336

namespace decoder
{
	class EntropyController
	{
	private:
		Streamer * streamObject;
		ProbabilityModelSelection * probMods;
		ArithmeticDecoder aritDecod;

		ctxStruct4ALG1 ctxDataALG1;
		bool firstTime = true;
		unsigned char activeSubBlockScanIdx = 0;
		unsigned char ctxSet;
		unsigned char greater1Ctx;
		unsigned char greater1CtxPrevSubBlock = 0;

		int DecodeElement(syntaxElemEnum syntaxElemNo);
		void DecodeHeader();
		void DecodeVPS();
		void DecodeSPS();
		void DecodePPS();
		void DecodeProfileTierLevel();
		void DecodeVUI();
		void DecodeStRefPicSet(int numOfShortRefPicSets);
		void DecodeSliceSegment();
		void DecodeSliceSegmentHeader();
		void DecodeCTU(Partition *partition, CUContext * cuCtx, int i);
		void DecodeQuadtree(CU *cu, int depth, CUContext* cuCtx, int x, int y, bool splitInf, int partitionWidth, int partitionHeight);
		void DecodeCU(CU * cu, CUContext * cuCtx, int partitionWidth);
		void DecodePU(PU *pu);
		void DecodeMVD(PU * pu);
		void DecodeTransformTree(TU * transformTree, CUContext * cuCtx, CU *cu, int partitionWidth);
		void DecodeTU(CUContext * cuCtx, TU * tu, CU *cu);
		void  DecodeTB(TB *transformBlock, CUContext * cuCtx);
		int FromBitstream(syntaxElemEnum syntaxElemNo);

		/* Calculate ctxIdx from ctxOffset and ctxInc when last value depends on side information */
		unsigned short derivateSpecCtxIdx(int ctxIncTableIdx, int i);

		/* Derivate ctxinc from side information */
		unsigned char derivateCtxInc_split_cu_flag(unsigned char cqtDepth, unsigned char ctDepthNbL, unsigned char ctDepthNbA, bool availableL, bool availableA);
		unsigned char derivateCtxInc_cu_skip_flag(bool cu_skip_flagNbL, bool cu_skip_flagNbA, bool availableL, bool availableA);
		unsigned char derivateCtxIncCSBF(unsigned short cIdx, bool csbfRight, bool csbfBottom, unsigned char xS, unsigned char yS, unsigned short log2TrafoSize);
		unsigned char derivateCtxIncLAST(unsigned short binIdx, unsigned short cIdx, unsigned short log2TrafoSize);
		unsigned char derivateCtxIncSIG(unsigned short cIdx, unsigned char xC, unsigned char yC, unsigned char scanIdx, unsigned short log2TrafoSize, bool csbfRight, bool csbfBottom,
			bool transform_skip_context_enabled_flag, bool transform_skip_flag, bool cu_transquant_bypass_flag);
		ctxStruct4ALG1 derivateCtxIncALG1(unsigned short cIdx, unsigned char subblockScanIdx, unsigned char coeffScanIdx, bool lastALG1);
		unsigned char derivateCtxIncALG2(unsigned short cIdx, unsigned char ctxSet);

	public:
		ContextMemory ctxMem;
		EncodingContext	*decCtx;

		EntropyController();
		~EntropyController();

		void Init(Streamer * streamObject, EncodingContext *_decCtx);

		void DecodeNextNAL();
		void DecodeSliceSegmentData(Partition * partition);
		void RBSPSliceSegmentTrailingBits();
		bool IsVideoDecoded();
		void ReleaseMemory();

		long GetBaseOffset();
		void EndFrame();
		void clearModels();



	};
}
