/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once

#include "BlockPartition.h"
#include "Frame.h"
#include "Enums.h"
#include <cmath>

namespace decoder
{
	class CTBController
	{
	public:
		CTBController();
		~CTBController();

		void Init(EncodingContext * _decCtx);
		void ReconstructCTBTree(Partition * partition);
		void CreateRelations(Partition * partition);

	private:
		EncodingContext *decCtx;

		void ReconstructChildren(CU * cu, int width, int height, int frameWidth, int frameHeight);
		void ReconstructTransformTree(CU * cu, int width, int height, int frameWidth, int frameHeight);
		void ReconnstructTransformUnit(TU * tu, int width, int height, int frameWidth, int frameHeight);
		void setFlags(CU *cu, int frameWidth, int frameHeight);
	};
}