# Bolt65

Bolt65 is a codename for an ongoing project on Faculty of Electrical Engineering and Computing in Zagreb. The aim of the project is to develop a “clean room” software/hardware suite consisting of an encoder, decoder, and transcoder. Special focus is set on the performance-efficiency achieved by low-level optimizations and hardware-software co-design by exploiting heterogeneous accelerator-based architectures. Another important focus of Bolt65 is the just-in-time requirement which sets constraints on processing time making Bolt65 suitable for encoding/transcoding on demand. The project is intended for research and development of current and future video processing algorithms and high-performance computing architectures. Bolt65 suite is written from scratch in C++ and is supported on Windows and Linux operating systems.

### **Repository structure**

The example sources that use Bolt65 libraries are located in folder src/.
All headers with the definitions are located in folder include/. 
Static and dynamic libraries built for different architectures, with different vector extensions are located in lib/ folder. 

The initial data needed to test the application is located in data/ folder. It contains one example raw video sequence, and default configurations. 
In build/ folder, there is an example of building the application for linux (Makefile) and Windows (Visual studio solution).

### **Compiling the application** 

There are static and dynamic libraries built for Windows and Linux OS, on different architectures (x64 and arm). 

The example application that uses Bolt65 encoder, decoder, and transcoder can be built on Windows and Linux: 

* In build/vc2017/Bolt65 there is Bolt65.sln file that can be opened in Visual Studio 2017 (works with Visual C++ and Intel compilers)
* In build/linux there is a Makefile that can be used to build Bolt encoder, decoder, and transcoder


### **Running the application** 

The applications (encoder, decoder, transcoder) can be run several ways: 

	1.	By defining all options in configuration file and passing only the path of the configuration file to the application 

		Example (examples of configuration file can be found in /data/configurations/): 

			./bolt-encoder -c config.cfg

	2. By defining all options through command line separately. All the options are listed below
	
	3. By combining first two methods. In this case any option set in command line will override appropriate option defined in configuration file.


### **Input/Output options**

`--config, -c <filename>`

Use external config file.

---

`--input, -i <filename>`

Input filename, only raw YUV or Y4M supported.

---

`--output, -o <filename>`

Bitstream output file name.

The output will contain raw HEVC bitstream.

---

`--frames, -f <integer>`

Number of frames to be encoded.

---

`--picWidth, -w <integer>`

Input picture width. If input file is Y4M, picture width will be read from input file.

---

`--picHeight, -h <integer>`

Input picture Height. If input file is Y4M, picture height will be read from input file. 

---

`--framerate, -fr <integer>`

Source file frame rate.

---

### **Encoding info options**

`--qp, -q <integer>`

Specify base quantization parameter for Constant QP rate control. Default QP is 24. 

`--gop <string>`

Define GOP structure of output HEVC bitstream. I or P frames. If not defined All INTRA configuration will be used.

### **Algorithms**

## Search algorithms

Default algorithm is Three Step Search with search area of 6 pixels

`--full-integer-search`

Use full integer search in interprediction.

---

`--full-fractional-search`

Use full fractional search in interprediction.

---

`--three-step-search`

Use three step search in interprediction.

---

`--search-area <integer>`

Search area for full integer and fractional search.

---

## Interpolation algorithms

Default interpolation algorithm is standard HEVC 7/8 tap filter for luma and 4 tap filter for chroma. 

`--hevc-interpolation`

Use standard HEVC interpolation.

---

`--hevc-sf5`

Use standard HEVC interpolation with SF = 5. 

---

`--avc-interpolation`

Use interpolation as in previous AVC standard (6-tap filter)

---

`--bilinear-interpolation`

Use bilinear interpolation.

---

`--four-tap-interpolation`

Use 4-tap filter interpolation.

---

## Block matching algorithms

Default block matching algorithm is Sum of Absolute Differences (SAD)

`--sad`

Use Sum of Absoulte Differences

---

`--satd`

Use Sum of Absoulte Transform Differences (Fast Walsh–Hadamard transform algorithm)

---

`--mse`

Using Mean Square Error algorithm.

---

### **Flags**

## In-loop filters 

`--enable-sao-filter, --disable-sao-filter>`

Enable or disable SAO filtering.

---

`--enable-deblocking-filter, --disable-deblocking-filter>`

Enable or disable Deblocking filtering.

## Prediction units

`--enable-smp, --disable-smp`

Enable or disable Symetric Motion Prediction units.

---

`--enable-amp, --disable-amp`

Enable or disable Asymetric Motion Prediction units.

---

`--all-modes`

Both INTRA and INTER prediction will be evaluated for Coding units in P frames.

---

## Architectures

`--avx`

Enable Advances Vector eXtensions (AVX2) if the current architecture supports it. 

---

`--neon`

Enable ARM NEON vector extensions if the current architecture supports it. 

---

`--sve`

Enable ARM SVE vector extensions if the current architecture supports it. 

---

`--epi-riscv`

Enable EPI RISC-V vector builtins if the current architecture supports it. 

---

### Multithreading

`--threads <integer>`

Define number of threads that will be used. Threads can be utilized only if number of tiles is larger than 1.

---

## Tiles

Tiles can be se uniformly or nonuniformly 

`--tile-rows <integer>`

Specify number of tiles in a row of frame. Height of each row will be calculated uniformly.

`--tile-columns <integer>`

Specify number of tiles in a column of frame. Width of each column will be calculated uniformly.

---

`--tile-rows <integer>;<integer>;<integer>;...`

Specify size of each tile in a row (as number of CTU's).

`--tile-columns <integer>;<integer>;<integer>;...`

Specify size of each tile in a column (as number of CTU's).


### **Debugging and statistics**

`--csv <filename>`

Outputs statistical information to file.

---

`--csv-tiles <filename>`

Outputs statistical information abotu tiles to file.

---

`--calculate-bitrate`

Calculate output video bitrate.

---

`--calculate-psnr`

Calculate output video Peak Signal to Nose Ratio for Y, U and V planes.

---

`--calculate-time`

Calculate time needed for video processing.

---

`--show-stats-per-frame`

Shows selected statistics after each frame

---

`--show-stats-per-tile`

Shows selected statistics after each tile in frame

---
