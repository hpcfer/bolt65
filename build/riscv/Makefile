BOLT_SRC_PATH=../../src
BOLT_INCLUDE_PATH=../../include

EPI_TOOLCHAIN=/shared_folder/llvm-EPI-0.7-development-toolchain-native/
CXX = $(EPI_TOOLCHAIN)/bin/clang++

# Available vector extensions: "riscv-epi"
VEC_EXT=

STATIC_LIBS_OUT=../../libs/riscv/static/$(VEC_EXT)
DYNAMIC_LIBS_OUT=../../libs/riscv/dynamic/$(VEC_EXT)


CXXFLAGS:=-mepi -g -std=gnu++11 -w -pthread -DNDEBUG -O3 -D__EPI_RISCV

#########Main source files#########
MAIN_ENCODER=$(BOLT_SRC_PATH)/Bolt65-encoder.cpp
MAIN_DECODER=$(BOLT_SRC_PATH)/Bolt65-decoder.cpp
MAIN_TRANSCODER=$(BOLT_SRC_PATH)/Bolt65-transcoder.cpp

#########Inlcude directories and libraries#########
INC_DIRS_COMMON= -I $(BOLT_INCLUDE_PATH)/Bolt65-common
INC_DIRS_ENCODER= -I $(BOLT_INCLUDE_PATH)/Bolt65-encoder-lib -I $(BOLT_INCLUDE_PATH)/Bolt65-common
INC_DIRS_DECODER= -I $(BOLT_INCLUDE_PATH)/Bolt65-decoder-lib -I $(BOLT_INCLUDE_PATH)/Bolt65-common
INC_DIRS_TRANSCODER= -I $(BOLT_INCLUDE_PATH)/Bolt65-decoder-lib -I $(BOLT_INCLUDE_PATH)/Bolt65-decoder-lib -I $(BOLT_INCLUDE_PATH)/Bolt65-transcoder-lib -I $(BOLT_INCLUDE_PATH)/Bolt65-common

STATIC_LIBS_PATH=-L$(STATIC_LIBS_OUT)
DYNAMIC_LIBS_PATH=-L$(DYNAMIC_LIBS_OUT)

LIBS_ENCODER=-lBolt65-encoder -lBolt65-common -Wl,--whole-archive -lpthread -Wl,--no-whole-archive
LIBS_DECODER=-lBolt65-decoder -lBolt65-common -Wl,--whole-archive -lpthread -Wl,--no-whole-archive
LIBS_TRANSCODER=-lBolt65-transcoder -lBolt65-encoder -lBolt65-decoder -lBolt65-common -Wl,--whole-archive -lpthread -Wl,--no-whole-archive

.PHONY: clean all bolt-static bolt-dynamic
all: bolt-static bolt-dynamic

bolt-static: bolt-encoder-static bolt-decoder-static bolt-transcoder-static
bolt-dynamic: bolt-encoder-dynamic bolt-decoder-dynamic bolt-transcoder-dynamic

bolt-encoder-static: 
	$(CXX) $(MAIN_ENCODER) $^ -static $(CXXFLAGS) $(INC_DIRS_ENCODER) $(STATIC_LIBS_PATH) $(LIBS_ENCODER) -o $@

bolt-decoder-static:
	$(CXX) $(MAIN_DECODER) $^ -static $(CXXFLAGS) $(INC_DIRS_DECODER) $(STATIC_LIBS_PATH) $(LIBS_DECODER) -o $@

bolt-transcoder-static:
	$(CXX) $(MAIN_TRANSCODER) $^ -static $(CXXFLAGS) $(INC_DIRS_TRANSCODER) $(STATIC_LIBS_PATH) $(LIBS_TRANSCODER) -o $@

bolt-encoder-dynamic: 
	$(CXX) $(MAIN_ENCODER) -fPIC $^ $(CXXFLAGS) $(INC_DIRS_ENCODER) $(DYNAMIC_LIBS_PATH) $(LIBS_ENCODER) -o $@

bolt-decoder-dynamic:
	$(CXX) $(MAIN_DECODER) -fPIC  $^ $(CXXFLAGS) $(INC_DIRS_DECODER) $(DYNAMIC_LIBS_PATH) $(LIBS_DECODER) -o $@

bolt-transcoder-dynamic:
	$(CXX) $(MAIN_TRANSCODER) -fPIC $^ $(CXXFLAGS) $(INC_DIRS_TRANSCODER) $(DYNAMIC_LIBS_PATH) $(LIBS_TRANSCODER) -o $@

clean: 
	rm -f bolt-encoder-static
	rm -f bolt-decoder-static
	rm -f bolt-transcoder-static
	rm -f bolt-encoder-dynamic
	rm -f bolt-decoder-dynamic
	rm -f bolt-transcoder-dynamic